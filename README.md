# README #

Debris - Physically-based rendering engine optimized for movie production

Debris is a photo-realistic physically-based rendering engine designed to meet the demand requirements of visual effects artists and movie production studios. It is developed from the ground up using the state of the art rendering techniques and software programming methods. Debris is currently under active development.
