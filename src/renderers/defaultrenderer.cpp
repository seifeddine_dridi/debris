#include "stdafx_core.h"
#include <defaultrenderer.h>

#include <core/scene.h>
#include <core/lightintegrator.h>

NAMESPACE_DEBRIS_BEGIN

DefaultRenderer::DefaultRenderer(const shared_ptr<LightIntegrator> &integrator) : integrator(integrator) {}

void DefaultRenderer::renderFrame(const shared_ptr<Scene> &scene, float time, int spp, const ImageSlice &tile) {
	Random rng;
	rng.seed(time * 1000.0f);
	shared_ptr<Camera> &camera = scene->getCamera();
	shared_ptr<Film> &film = camera->getFilm();
	for (int s = 0; s < spp; ++s) {
		float t = rng.nextFloat();
		for (int y = tile.ystart; y <= tile.yend; ++y) {
			for (int x = tile.xstart; x <= tile.xend; ++x) {
				// Sample wavelength [380..780]
				const float minW = 380.0f, maxW = 780.0f;
				float wavelength = rng.nextFloat() * (maxW - minW) + minW;
				Ray ray = camera->project(x + rng.nextFloat(), y + rng.nextFloat(), rng.nextFloat(), rng.nextFloat(), t);
				XYZColor radiance = integrator->getRadiance(ray, scene, wavelength, rng);
				film->add(x, y, radiance / spp);
			}
		}
	}
}

NAMESPACE_DEBRIS_END
