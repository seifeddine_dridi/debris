#include <iostream>
#include <sstream>

#include <capi/enginecwrapper.h>

int main(int argc, char **argv) {
	if (argc < 3 || argc > 7) {
		std::cout << "DebrisCLI [options] <input_scene> <output_image>" << std::endl << std::endl;
		std::cout << "Debris is an experimental photo-realistic rendering engine "
			"that aims to deliver the best rendering performance while maintaining good image quality." << std::endl << std::endl;
		std::cout << "Version: 0.1RC" << std::endl << std::endl;
		std::cout << "Options:" << std::endl;
		std::cout << "\t-spp: samples per pixel (default 1)" << std::endl;
		std::cout << "\t-nthreads (default auto): number of threads to use during rendering" << std::endl;
		return -1;
	}
	int spp = 1;
	unsigned nbThreads = 0;
	for (int i = 1; i < argc - 1; ++i) {
		if (!strcmp(argv[i], "-spp")) {
			std::istringstream stream(argv[i + 1]);
			stream >> spp;
		} else if (!strcmp(argv[i], "-nthreads")) {
			std::istringstream stream(argv[i + 1]);
			stream >> nbThreads;
		}
	}
	if (!nbThreads) {
		nbThreads = getMaximumThreadCount();
	}
	renderFrameFromSceneFile(argv[argc - 2], argv[argc - 1], spp, nbThreads);
	return 0;
}
