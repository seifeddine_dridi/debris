#include "stdafx_core.h"
#include <sphere.h>

NAMESPACE_DEBRIS_BEGIN

Sphere::Sphere(const Vector &position, float radius, const Transform &transform) :
	position(position), radius(radius) {
	this->local2world = transform;
	this->local2world.translate(position);
	this->interpolatedTransform = InterpolatedTransform(local2world);
	this->currentTime = 0.0f;
}

Sphere::~Sphere() {}

bool Sphere::intersect(RTCRay& ray) const {
	Transform l2w = interpolatedTransform.interpolate(currentTime + 5.0f * ray.time);
	const Transform &world2local = l2w.getInverse();
	Ray tRay = world2local * toRay(ray);
	Vector relpos = tRay.o;
	float A = tRay.d.lengthSquared();
	float B = 2.0f * relpos.dot(tRay.d);
	float C = relpos.lengthSquared() - radius * radius;
	float determinant = B * B - 4.0f * A * C;
	if (determinant < 0.0f) {
		return false; 
	}
	float distSqrt = sqrtf(determinant);
	float q;
	if (B < 0) {
		q = (-B - distSqrt) * 0.5f;
	} else {
		q = (-B + distSqrt) * 0.5f;
	}
	float t0 = q / A;
	float t1 = C / q;

	if (t0 > t1) {
		float temp = t0;
		t0 = t1;
		t1 = temp;
	}
	if (t1 < 0) {
		return false;
	}
	float t;
	if (t0 < 0) {
		t = t1;
	} else {
		t = t0;
	}
	if (t < tRay.mint || t > tRay.maxt) {
		return false;
	}
	Vector localPos = tRay(t);
	Vector normal = (l2w * localPos - l2w * Vector()) / radius;
	ray.Ng[0] = normal[0];
	ray.Ng[1] = normal[1];
	ray.Ng[2] = normal[2];
	ray.tfar = t;
	float phi = atan2(localPos.y, localPos.x);
	if (phi < 0) {
		phi += 2.0f * M_PI;
	}
	ray.u = phi / (2.0f * M_PI);
	ray.v = (1.0f - localPos.z / radius) / 2.0f;
	ray.geomID = geomID;
	return true;
}

bool Sphere::intersectP(RTCRay& ray) const {
	Transform l2w = interpolatedTransform.interpolate(currentTime + 5.0f * ray.time);
	Transform world2local = l2w.getInverse();
	Ray tRay = world2local * toRay(ray);
	Vector relpos = tRay.o;
	float A = tRay.d.lengthSquared();
	float B = 2.0f * relpos.dot(tRay.d);
	float C = relpos.lengthSquared() - radius * radius;
	float determinant = B * B - 4.0f * A * C;
	if (determinant < 0.0f) {
		return false;
	}
	float distSqrt = sqrtf(determinant);
	float q;
	if (B < 0) {
		q = (-B - distSqrt) * 0.5f;
	}
	else {
		q = (-B + distSqrt) * 0.5f;
	}
	float t0 = q / A;
	float t1 = C / q;

	if (t0 > t1) {
		float temp = t0;
		t0 = t1;
		t1 = temp;
	}
	if (t1 < 0) {
		return false;
	}
	float t;
	if (t0 < 0) {
		t = t1;
	}
	else {
		t = t0;
	}
	if (t < tRay.mint || t > tRay.maxt) {
		return false;
	}
	ray.geomID = geomID;
	return true;
}

bool Sphere::isInside(const Vector &p) const {
	Transform world2local = local2world.getInverse();
	Vector relpos = world2local * p;
	return (relpos.dot(relpos) - radius * radius) > 0.0f;
}

Vector Sphere::sampleArea(float u1, float u2, float &pdf) const {
	// Uniform sampling
	pdf = 1.0f / (radius * radius * M_PI * 4.0f);
	float z = 1.0f - 2.0f * u2;
	float sint = sqrtf(1.0f - z * z);
	float x = cosf(u1 * 2.0f * M_PI) * sint;
	float y = sinf(u1 * 2.0f * M_PI) * sint;
	return local2world * (Vector(x, y, z) * radius);
}

Vector Sphere::getNormal(const Vector &point) const {
	return (point - local2world * Vector()) / radius;
}

void sphereBoundsFunc(const Sphere* spheres, size_t item, RTCBounds* bounds_o) {
	const Sphere& sphere = spheres[item];
	float radius = sphere.getRadius();
	Vector position = sphere.getPosition();
	bounds_o->lower_x = position.x - radius;
	bounds_o->lower_y = position.y - radius;
	bounds_o->lower_z = position.z - radius;
	bounds_o->upper_x = position.x + radius;
	bounds_o->upper_y = position.y + radius;
	bounds_o->upper_z = position.z + radius;
}

void sphereIntersectFunc(const Sphere* spheres, RTCRay& ray, size_t item) {
	if (spheres->intersect(ray)) {
		ray.primID = item;
	}
}

void sphereOccludedFunc(const Sphere* spheres, RTCRay& ray, size_t item) {
	if (spheres->intersectP(ray)) {
		ray.primID = item;
	}
}

void Sphere::registerShape(RTCScene &scene) const {
	geomID = rtcNewUserGeometry(scene, 1);
	rtcSetUserData(scene, geomID, reinterpret_cast<void*>(const_cast<Sphere*>(this)));
	rtcSetBoundsFunction(scene, geomID, (RTCBoundsFunc) &sphereBoundsFunc);
	rtcSetIntersectFunction(scene, geomID, (RTCIntersectFunc) &sphereIntersectFunc);
	rtcSetOccludedFunction(scene, geomID, (RTCOccludedFunc) &sphereOccludedFunc);
}

void Sphere::updateShape(const RTCScene &scene, float time) const {
	currentTime = time;
}

NAMESPACE_DEBRIS_END
