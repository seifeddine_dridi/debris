#include "stdafx_core.h"
#include <triangle.h>

NAMESPACE_DEBRIS_BEGIN

/* vertex and triangle layout */
struct Vertex { float x, y, z, r; };
struct TriangleIndices { int v0, v1, v2; };

Triangle::Triangle(const Transform &transform, Vector *vertices, Eigen::Vector2f *uvs) {
	this->vertices[0] = vertices[0];
	this->vertices[1] = vertices[1];
	this->vertices[2] = vertices[2];
	this->local2world = transform;
	// Transform vertices into local space
	worldToLocal();
	this->interpolatedTransform = InterpolatedTransform(local2world);
	if (uvs != nullptr) {
		this->uvs[0] = uvs[0];
		this->uvs[1] = uvs[1];
		this->uvs[2] = uvs[2];
		hasUVCoordinates = true;
	} else {
		hasUVCoordinates = false;
	}
	// Compute edges 
	Vector edges[2];
	edges[0] = this->vertices[1] - this->vertices[0];
	edges[1] = this->vertices[2] - this->vertices[0];
	// Compute triangle area
	triangle_area = (edges[0] % edges[1]).length() * 0.5f;
	inv_triangle_area = 1.0f / triangle_area;
}

void Triangle::worldToLocal() {
	Vector barycenter = (vertices[0] + vertices[1] + vertices[2]) / 3.0f;
	this->vertices[0] = vertices[0] - barycenter;
	this->vertices[1] = vertices[1] - barycenter;
	this->vertices[2] = vertices[2] - barycenter;
	local2world.translate(barycenter);
}

bool Triangle::intersect(RTCRay &ray) const {
	return false;
}

bool Triangle::intersectP(RTCRay &ray) const {
	return false;
}

void Triangle::postIntersect(const RTCRay &ray, DifferentialGeometry &dg) const {
	AbstractShape::postIntersect(ray, dg);
	// Double-sided triangle
	if (dg.normal.dot(Vector(ray.dir[0], ray.dir[1], ray.dir[2])) > 0) {
		dg.normal = -dg.normal;
	}
	if (hasUVCoordinates) {
		Eigen::Vector2f interpV = uvs[0] + (uvs[1] - uvs[0]) * ray.u + (uvs[2] - uvs[0]) * ray.v;
		dg.uv[0] = interpV.x();
		dg.uv[1] = interpV.y();
	}
}

bool Triangle::isInside(const Vector &p) const {
	return false;
}

Vector Triangle::sampleArea(float u1, float u2, float &pdf) const {
	float s = sqrtf(u1);
	float beta = 1.0f - s;
	float gamma = u2 * s;
	pdf = inv_triangle_area;
	Vector edges[2];
	// Compute edges 
	edges[0] = vertices[1] - vertices[0];
	edges[1] = vertices[2] - vertices[0];
	return local2world * (vertices[0] + edges[0] * beta + edges[1] * gamma);
}

Vector Triangle::getNormal(const Vector &point) const {
	// Apply transform
	Vector v0 = local2world * this->vertices[0];
	Vector v1 = local2world * this->vertices[1];
	Vector v2 = local2world * this->vertices[2];

	Vector edges[2];
	// Compute edges 
	edges[0] = v1 - v0;
	edges[1] = v2 - v0;

	// Compute normal
	Vector normal = (edges[1] % edges[0]).normalize();
	return normal;
}

void Triangle::registerShape(RTCScene &scene) const {
	if (interpolatedTransform.getTimeSpan() != 0) {
		/* Create a triangle with motion blur */
		geomID = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, 1, 3, 2);
		/* Set vertices at start time */
		Vertex* vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		for (int v = 0; v < 3; ++v) {
			Vector tvert = local2world * this->vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		/* Set vertices at end time */
		vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
		Transform endTransform = interpolatedTransform.interpolate(0.0f);
		for (int v = 0; v < 3; ++v) {
			Vector tvert = endTransform * this->vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
	} else {
		/* Create a normal triangle */
		geomID = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, 1, 3);
		// Apply transform
		Vertex* vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER);
		for (int v = 0; v < 3; ++v) {
			Vector tvert = local2world * this->vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER);
	}
	/* Set triangle indices */
	TriangleIndices* trs = (TriangleIndices*)rtcMapBuffer(scene, geomID, RTC_INDEX_BUFFER);
	trs[0].v0 = 0;
	trs[0].v1 = 1;
	trs[0].v2 = 2;
	rtcUnmapBuffer(scene, geomID, RTC_INDEX_BUFFER);
}

void Triangle::updateShape(const RTCScene &scene, float time) const {
	if (interpolatedTransform.getTimeSpan() != 0) {
		Transform startTransform, endTransform;
		interpolatedTransform.interpolate(time, startTransform, endTransform);
		/* Set vertices at start time */
		Vertex* vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		for (int v = 0; v < 3; ++v) {
			Vector tvert = startTransform * this->vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		/* Set vertices at end time */
		vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
		for (int v = 0; v < 3; ++v) {
			Vector tvert = endTransform * this->vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
		rtcUpdate(scene, geomID);
	}
}

NAMESPACE_DEBRIS_END
