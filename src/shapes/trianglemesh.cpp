#include "stdafx_core.h"
#include <trianglemesh.h>

NAMESPACE_DEBRIS_BEGIN

/* vertex and triangle layout */
struct Vertex { float x, y, z, r; };
struct TriangleIndices { int v0, v1, v2; };

TriangleMesh::TriangleMesh(const Transform &transform, const Vector *vertices, const int *triangles,
	float *uvs, int nbVertices, int nbTriangles) : nbVertices(nbVertices), nbTriangles(nbTriangles) {
	this->local2world = transform;
	Vector center;
	this->vertices = new Vector[nbVertices];
	for (int i = 0; i < nbVertices; ++i) {
		center += vertices[i];
		this->vertices[i] = vertices[i];
	}
	center /= (float)nbVertices;
	local2world.translate(center);
	this->interpolatedTransform = InterpolatedTransform(local2world);
	if (uvs) {
		this->uvs = new float[nbVertices * 2];
		hasUVCoordinates = true;
		for (int i = 0; i < 2 * nbVertices; ++i) {
			this->uvs[i] = uvs[i];
		}
	} else {
		this->uvs = nullptr;
		hasUVCoordinates = false;
	}
	for (int v = 0; v < nbVertices; ++v) {
		this->vertices[v] = this->vertices[v] - center;
	}
	this->triangles = new int[nbTriangles * 3];
	for (int f = 0; f < 3 * nbTriangles; ++f) {
		this->triangles[f] = triangles[f];
	}
}

TriangleMesh::~TriangleMesh() {
	delete[] vertices;
	delete[] uvs;
	delete[] triangles;
}

bool TriangleMesh::intersect(RTCRay& ray) const {
	return false;
}

bool TriangleMesh::intersectP(RTCRay& ray) const {
	return false;
}

void TriangleMesh::postIntersect(const RTCRay &ray, DifferentialGeometry &dg) const {
	AbstractShape::postIntersect(ray, dg);
	if (dg.normal.dot(Vector(ray.dir[0], ray.dir[1], ray.dir[2])) > 0) {
		dg.normal = -dg.normal;
	}
	if (hasUVCoordinates) {
		int v0 = triangles[ray.primID * 3];
		int v1 = triangles[ray.primID * 3 + 1];
		int v2 = triangles[ray.primID * 3 + 2];
		Vector uvVertex0(uvs[v0 * 2], uvs[v0 * 2 + 1]);
		Vector uvVertex1(uvs[v1 * 2], uvs[v1 * 2 + 1]);
		Vector uvVertex2(uvs[v2 * 2], uvs[v2 * 2 + 1]);
		Vector interpV = uvVertex0 + (uvVertex1 - uvVertex0) * ray.u + (uvVertex2 - uvVertex0) * ray.v;
		dg.uv[0] = interpV.x;
		dg.uv[1] = interpV.y;
	}
}

bool TriangleMesh::isInside(const Vector &p) const {
	return false;
}

Vector TriangleMesh::sampleArea(float u1, float u2, float &pdf) const {
	return Vector();
}

Vector TriangleMesh::getNormal(const Vector &point) const {
	return Vector();
}

void TriangleMesh::registerShape(RTCScene &scene) const {
	if (interpolatedTransform.getTimeSpan() != 0) {
		/* Create a triangle mesh with motion blur */
		geomID = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, nbTriangles, nbVertices, 2);
		/* Set vertices at start time */
		Vertex* vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		for (int v = 0; v < nbVertices; ++v) {
			Vector tvert = local2world * vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		/* Set vertices at end time */
		vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
		//const Transform &endTransform = interpolatedTransform.getEndTransform();
		Transform endTransform = interpolatedTransform.interpolate(5.0f);
		for (int v = 0; v < nbVertices; ++v) {
			Vector tvert = endTransform * vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
	} else {
		/* Create a normal triangle mesh */
		geomID = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, nbTriangles, nbVertices);
		Vertex* vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER);
		for (int v = 0; v < nbVertices; ++v) {
			Vector tvert = local2world * this->vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER);
	}
	/* Set triangle indices */
	TriangleIndices* trs = (TriangleIndices*)rtcMapBuffer(scene, geomID, RTC_INDEX_BUFFER);
	for (int f = 0; f < nbTriangles; ++f) {
		trs[f].v0 = triangles[3 * f];
		trs[f].v1 = triangles[3 * f + 1];
		trs[f].v2 = triangles[3 * f + 2];
	}
	rtcUnmapBuffer(scene, geomID, RTC_INDEX_BUFFER);
}

void TriangleMesh::updateShape(const RTCScene &scene, float time) const {
	if (interpolatedTransform.getTimeSpan() != 0) {
		Transform startTransform, endTransform;
		interpolatedTransform.interpolate(time, startTransform, endTransform);
		/* Set vertices at start time */
		Vertex* vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		for (int v = 0; v < nbVertices; ++v) {
			Vector tvert = startTransform * vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER0);
		/* Set vertices at end time */
		vert = (Vertex*)rtcMapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
		//const Transform &endTransform = interpolatedTransform.getEndTransform();
		for (int v = 0; v < nbVertices; ++v) {
			Vector tvert = endTransform * vertices[v];
			vert[v].x = tvert.x;
			vert[v].y = tvert.y;
			vert[v].z = tvert.z;
		}
		rtcUnmapBuffer(scene, geomID, RTC_VERTEX_BUFFER1);
		rtcUpdate(scene, geomID);
	}
}

NAMESPACE_DEBRIS_END
