#include "stdafx_core.h"
#include <texture.h>

#include <OpenImageIO/imageio.h>

NAMESPACE_DEBRIS_BEGIN

UniformTexture::UniformTexture(const Vector &rgb) {
	color = spectral::rgb2xyz(rgb);
}

UniformTexture::UniformTexture(float wavelength, float value) {
	color = spectral::toXYZ(wavelength, value);
}

XYZColor UniformTexture::getColor(float u, float v) const {
	return color;
}

CheckerBoard::CheckerBoard(float sizex, float sizey) : sizex(sizex), sizey(sizey) {}

XYZColor CheckerBoard::getColor(float u, float v) const {
	int x = (int)(u / sizex);
	int y = (int)(v / sizey);
	if ((x & 1 && !(y & 1)) || (!(x & 1) && y & 1)) {
		return XYZColor(0.8f, 0.8f, 0.8f);
	}
	return 0.0f;
}

ImageTexture::ImageTexture(const std::string &imagesrc) : imageBuf(imagesrc) {
	if (imageBuf.has_error()) {
		throw DebrisException(std::string("Could not load image: " + imagesrc));
	}
	imageSpec = imageBuf.spec();
}

ImageTexture::~ImageTexture() {
}

XYZColor ImageTexture::getColor(float u, float v) const {
	int x = (int)(u * (imageSpec.width - 1.0f));
	int y = (int)(v * (imageSpec.height - 1.0f));
	float rgb[3] = { 0, 0, 0 };
	imageBuf.getpixel(x, y, rgb);
	return spectral::rgb2xyz(Vector(rgb[0], rgb[1], rgb[2]));
}

NAMESPACE_DEBRIS_END
