#include "stdafx_core.h"
#include <propertylist.h>

NAMESPACE_DEBRIS_BEGIN

#define DEFINE_PROPERTY_ACCESSOR(Type, TypeName) \
	void PropertyList::add##TypeName(const std::string &property, const Type &value) { \
		if (props.find(property) != props.end()) {\
			std::cerr << "Property \"" << property <<  "\" was specified multiple times!" << std::endl; \
		}\
		props[property] = value; \
	} \
	\
	Type PropertyList::get##TypeName(const std::string &property) const { \
		std::map<std::string, Variant>::const_iterator it = props.find(property); \
		if (it == props.end()) { \
			throw DebrisException("Property " + property + " was not found"); \
		} \
		const Type *result = boost::get<Type>(&it->second); \
		if (!result) \
			throw DebrisException("Property " + property +  " has a wrong type"); \
		return (Type) *result; \
	} \
	\
	Type PropertyList::get##TypeName(const std::string &property, const Type &defaultValue) const { \
		std::map<std::string, Variant>::const_iterator it = props.find(property); \
		if (it == props.end()) { \
			return defaultValue; \
		} \
		const Type *result = boost::get<Type>(&it->second); \
		if (!result) \
			throw DebrisException("Property " + property +  " has a wrong type"); \
		return (Type) *result; \
	} \

DEFINE_PROPERTY_ACCESSOR(int, Int);
DEFINE_PROPERTY_ACCESSOR(float, Float);
DEFINE_PROPERTY_ACCESSOR(std::string, String);
DEFINE_PROPERTY_ACCESSOR(AffineSpace, AffineSpace);

NAMESPACE_DEBRIS_END
