#include "stdafx_core.h"
#include <taskmanager.h>

#include <boost/bind.hpp>

#include <task.h>

NAMESPACE_DEBRIS_BEGIN

TaskManager::TaskManager(unsigned nbThreads) : executionState(IDLE), work(service), nbTasks(0), tasksStatistics(this) {
	//const int nbThreads = boost::thread::hardware_concurrency();
	for (int i = 0; i < nbThreads; ++i) {
		threadpool.create_thread(boost::bind(&boost::asio::io_service::run, &service));
	}
}

TaskManager::~TaskManager() {
}

void TaskManager::postTask(Task *task) {
	boost::shared_ptr<Task> taskPtr(task);
	service.post(boost::bind(&Task::run, taskPtr));
	++nbTasks;
}

void TaskManager::execute() {
	executionState = TASKS_EXECUTION_STATE::RUNNING;
	// Stop accepting tasks
	service.stop();
	//threadpool.join_all();
}

void TaskManager::pause() {
	executionState = TASKS_EXECUTION_STATE::SUSPENDED;
}

void TaskManager::resume() {
	executionState = TASKS_EXECUTION_STATE::RUNNING;
	tasksStatistics.notifyTasks();
}

void TaskManager::finish() {
	executionState = TASKS_EXECUTION_STATE::FINISHED;
}

float TaskManager::getProgress() const {
	return tasksStatistics.getStatistics(TASK_STATISTICS::NB_FINISHED_TASKS) / (float)nbTasks;
}

TaskEventBroadcaster *TaskManager::getTaskEventBroadcaster() {
	return &tasksStatistics;
}

TASKS_EXECUTION_STATE TaskManager::getExecutionState() const {
	return executionState;
}

unsigned TaskManager::getNbTasks() const {
	return nbTasks;
}

NAMESPACE_DEBRIS_END
