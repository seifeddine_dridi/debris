#include "stdafx_core.h"
#include <taskeventbroadcaster.h>

#include <iostream>
#include <taskmanager.h>

NAMESPACE_DEBRIS_BEGIN

static boost::condition_variable conditionVariable;

TaskEventBroadcaster::TaskEventBroadcaster(TaskManager *taskManager) : taskManager(taskManager) {
	for (int i = 0; i < 3; ++i) {
		stats[i] = 0;
	}
}

void TaskEventBroadcaster::taskStarted(unsigned taskId) {
	boost::unique_lock<boost::mutex> lock(mutex);
	while (taskManager->getExecutionState() == TASKS_EXECUTION_STATE::SUSPENDED) {
		std::cout << "Threads were suspended" << std::endl;
		conditionVariable.wait(lock);
	}
	//boost::mutex::scoped_lock scopedLock(mutex);
	++stats[NB_STARTED_TASKS];
}

void TaskEventBroadcaster::taskEnded(unsigned taskId) {
	boost::mutex::scoped_lock scopedLock(mutex);
	++stats[NB_FINISHED_TASKS];
	std::cout << "Rendering progress " << 100.0f * stats[NB_FINISHED_TASKS] / taskManager->getNbTasks() << "\r";
	if (stats[NB_FINISHED_TASKS] == taskManager->getNbTasks()) {
		std::cout << std::endl;
		// Signal that all tasks have finished their execution.
		taskManager->finish();
	}
}

unsigned TaskEventBroadcaster::getStatistics(TASK_STATISTICS statType) const {
	return stats[statType];
}

void TaskEventBroadcaster::notifyTasks() {
	conditionVariable.notify_all();
}

NAMESPACE_DEBRIS_END
