#include "stdafx_core.h"
#include <scene.h>

NAMESPACE_DEBRIS_BEGIN

Scene::Scene(const PrimitiveList &primitives, const LightList &lights, const shared_ptr<Camera> &camera) :
	primitives(primitives), lights(lights), camera(camera) {
	rtcInit();
	for (shared_ptr<Light> light : lights) {
		if (light->hasInfiniteArea()) {
			envLight = light;
			break;
		}
	}
	scene = rtcNewScene(RTC_SCENE_DYNAMIC, RTC_INTERSECT1);
	for (const shared_ptr<Primitive> &prim : primitives) {
		prim->getShape()->registerShape(scene);
	}
	rtcCommit(scene);
}

Scene::~Scene() {
	rtcExit();
}

void Scene::addPrimitive(const shared_ptr<Primitive> &primitive) {
	primitives.push_back(primitive);
}

void Scene::addLight(const shared_ptr<Light> &light) {
	lights.push_back(light);
}

bool Scene::intersect(const Ray &ray, IntersectionRecord *info) const {
	RTCRay eRay;
	eRay.org[0] = ray.o[0];
	eRay.org[1] = ray.o[1];
	eRay.org[2] = ray.o[2];
	eRay.dir[0] = ray.d[0];
	eRay.dir[1] = ray.d[1];
	eRay.dir[2] = ray.d[2];
	eRay.tnear = ray.mint;
	eRay.tfar = ray.maxt;
	eRay.time = ray.time;
	eRay.geomID = RTC_INVALID_GEOMETRY_ID;
	eRay.primID = RTC_INVALID_GEOMETRY_ID;
	eRay.mask = 0xFFFFFFFF;
	/* Intersect ray with scene */
	rtcIntersect(scene, eRay);
	if (eRay.geomID != RTC_INVALID_GEOMETRY_ID) {
		primitives[eRay.geomID]->postIntersection(eRay, info);
		return true;
	}
	return false;
}

bool Scene::intersectP(const Ray &ray) const {
	RTCRay eRay;
	eRay.org[0] = ray.o[0];
	eRay.org[1] = ray.o[1];
	eRay.org[2] = ray.o[2];
	eRay.dir[0] = ray.d[0];
	eRay.dir[1] = ray.d[1];
	eRay.dir[2] = ray.d[2];
	eRay.tnear = ray.mint;
	eRay.tfar = ray.maxt;
	eRay.time = ray.time;
	eRay.geomID = RTC_INVALID_GEOMETRY_ID;
	eRay.primID = RTC_INVALID_GEOMETRY_ID;
	eRay.mask = -1;
	/* Intersect ray with scene */
	rtcOccluded(scene, eRay);
	if (eRay.geomID != RTC_INVALID_GEOMETRY_ID) {
		return true;
	}
	return false;
}

void Scene::setCamera(const shared_ptr<Camera> &camera) {
	this->camera = camera;
}

shared_ptr<Camera> &Scene::getCamera() {
	return camera;
}

const shared_ptr<Light> &Scene::getBackgroundLight() const {
	return envLight;
}

XYZColor Scene::getBackgroundRadiance(const Ray &ray) const {
	if (envLight.use_count()) {
		return envLight->getEmittance(ray.d);
	}
	return 0;
}

void Scene::updateScene(float time) const {
	for (const shared_ptr<Primitive> &prim : primitives) {
		prim->getShape()->updateShape(scene, time);
	}
	rtcCommit(scene);
}

NAMESPACE_DEBRIS_END
