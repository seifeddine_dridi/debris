#include "stdafx_core.h"
#include <bbox.h>

NAMESPACE_DEBRIS_BEGIN

bool BBox::intersect(const Ray &ray, float *tnear, float *tfar) const {
	float tmin = -INFINITY, tmax = ray.maxt;
	for (int i = 0; i < 3; ++i) {
		float invRayDir = 1.0f / ray.d[i];
		float t0 = (pmin[i] - ray.o[i]) * invRayDir;
		float t1 = (pmax[i] - ray.o[i]) * invRayDir;

		if (t0 > t1) std::swap(t0, t1);
		tmin = std::max(tmin, t0);
		tmax = std::min(tmax, t1);
		if (tmin > tmax) {
			return false;
		}
	}
	if (tnear) {
		*tnear = tmin;
	}
	if (tfar) {
		*tfar = tmax;
	}
	return true;
}

NAMESPACE_DEBRIS_END
