#include "stdafx_core.h"
#include <transform.h>

NAMESPACE_DEBRIS_BEGIN

InterpolatedTransform::InterpolatedTransform() : startTime(0), endTime(0) {
}

InterpolatedTransform::InterpolatedTransform(const Transform &startTransform) :
	startTransform(startTransform), endTransform(startTransform), startTime(0), endTime(0) {}

InterpolatedTransform::InterpolatedTransform(const Transform &startTransform, const AnimatedTransform &animation) :
	startTransform(startTransform) {
	startTime = animation.getStartTime();
	endTime = animation.getEndTime();
	endTransform = startTransform;
	float dt = endTime - startTime;
	endTransform.translate(animation.getTranslation() * dt);
	endTransform = endTransform * Eigen::AngleAxisf(animation.getRotation().angle() * dt, animation.getRotation().axis());
	this->animation = animation;
}

InterpolatedTransform::InterpolatedTransform(const Transform &startTransform,
	const AnimatedTransform &animation, int startTime, int endTime) {
	this->startTransform = startTransform;
	this->animation = animation;
	this->startTime = startTime;
	this->endTime = endTime;
	float dt = endTime - startTime;
	endTransform.translate(animation.getTranslation() * dt);
	endTransform = endTransform * Eigen::AngleAxisf(animation.getRotation().angle() * dt, animation.getRotation().axis());
}

Transform InterpolatedTransform::interpolate(float time) const {
	if (endTime - startTime == 0 || time <= startTime) {
		return startTransform;
	}
	else if (time >= endTime) {
		return endTransform;
	}
	Transform transInterp = startTransform;
	float dt = time - startTime;
	Vector newPosition = startTransform.getTranslation() + animation.getTranslation() * dt;
	transInterp.setTranslation(newPosition);
	return transInterp * Eigen::AngleAxisf(animation.getRotation().angle() * dt, animation.getRotation().axis());
}

void InterpolatedTransform::interpolate(float time, Transform &start, Transform &end) const {
	if (endTime - startTime == 0 || time <= startTime) {
		start = startTransform;
		end = startTransform;
		return;
	}
	else if (time >= endTime) {
		start = endTransform;
		end = endTransform;
		return;
	}
	start = interpolate(time);
	end = interpolate(time + 5.0f);
}

const Transform &InterpolatedTransform::getEndTransform() const {
	return endTransform;
}

float InterpolatedTransform::getNormalizedTimeValue(float time) const {
	if (endTime - startTime == 0 || time <= startTime) {
		return 0.0f;
	}
	else if (time >= endTime) {
		return 1.0f;
	}
	return (time - startTime) / (float)(endTime - startTime);
}

NAMESPACE_DEBRIS_END
