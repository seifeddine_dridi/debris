#include "stdafx_core.h"
#include <spectrum.h>

NAMESPACE_DEBRIS_BEGIN

#define RESOLUTION 81

namespace cie {
	/*! CIE XYZ response curves for the average human observer sampled at 5nm interval */
	static Vector curves[RESOLUTION] = {
		Vector(0.0014, 0.0000, 0.0065), Vector(0.0022, 0.0001, 0.0105), Vector(0.0042, 0.0001, 0.0201),
		Vector(0.0076, 0.0002, 0.0362), Vector(0.0143, 0.0004, 0.0679), Vector(0.0232, 0.0006, 0.1102),
		Vector(0.0435, 0.0012, 0.2074), Vector(0.0776, 0.0022, 0.3713), Vector(0.1344, 0.0040, 0.6456),
		Vector(0.2148, 0.0073, 1.0391), Vector(0.2839, 0.0116, 1.3856), Vector(0.3285, 0.0168, 1.6230),
		Vector(0.3483, 0.0230, 1.7471), Vector(0.3481, 0.0298, 1.7826), Vector(0.3362, 0.0380, 1.7721),
		Vector(0.3187, 0.0480, 1.7441), Vector(0.2908, 0.0600, 1.6692), Vector(0.2511, 0.0739, 1.5281),
		Vector(0.1954, 0.0910, 1.2876), Vector(0.1421, 0.1126, 1.0419), Vector(0.0956, 0.1390, 0.8130),
		Vector(0.0580, 0.1693, 0.6162), Vector(0.0320, 0.2080, 0.4652), Vector(0.0147, 0.2586, 0.3533),
		Vector(0.0049, 0.3230, 0.2720), Vector(0.0024, 0.4073, 0.2123), Vector(0.0093, 0.5030, 0.1582),
		Vector(0.0291, 0.6082, 0.1117), Vector(0.0633, 0.7100, 0.0782), Vector(0.1096, 0.7932, 0.0573),
		Vector(0.1655, 0.8620, 0.0422), Vector(0.2257, 0.9149, 0.0298), Vector(0.2904, 0.9540, 0.0203),
		Vector(0.3597, 0.9803, 0.0134), Vector(0.4334, 0.9950, 0.0087), Vector(0.5121, 1.0000, 0.0057),
		Vector(0.5945, 0.9950, 0.0039), Vector(0.6784, 0.9786, 0.0027), Vector(0.7621, 0.9520, 0.0021),
		Vector(0.8425, 0.9154, 0.0018), Vector(0.9163, 0.8700, 0.0017), Vector(0.9786, 0.8163, 0.0014),
		Vector(1.0263, 0.7570, 0.0011), Vector(1.0567, 0.6949, 0.0010), Vector(1.0622, 0.6310, 0.0008),
		Vector(1.0456, 0.5668, 0.0006), Vector(1.0026, 0.5030, 0.0003), Vector(0.9384, 0.4412, 0.0002),
		Vector(0.8544, 0.3810, 0.0002), Vector(0.7514, 0.3210, 0.0001), Vector(0.6424, 0.2650, 0.0000),
		Vector(0.5419, 0.2170, 0.0000), Vector(0.4479, 0.1750, 0.0000), Vector(0.3608, 0.1382, 0.0000),
		Vector(0.2835, 0.1070, 0.0000), Vector(0.2187, 0.0816, 0.0000), Vector(0.1649, 0.0610, 0.0000),
		Vector(0.1212, 0.0446, 0.0000), Vector(0.0874, 0.0320, 0.0000), Vector(0.0636, 0.0232, 0.0000),
		Vector(0.0468, 0.0170, 0.0000), Vector(0.0329, 0.0119, 0.0000), Vector(0.0227, 0.0082, 0.0000),
		Vector(0.0158, 0.0057, 0.0000), Vector(0.0114, 0.0041, 0.0000), Vector(0.0081, 0.0029, 0.0000),
		Vector(0.0058, 0.0021, 0.0000), Vector(0.0041, 0.0015, 0.0000), Vector(0.0029, 0.0010, 0.0000),
		Vector(0.0020, 0.0007, 0.0000), Vector(0.0014, 0.0005, 0.0000), Vector(0.0010, 0.0004, 0.0000),
		Vector(0.0007, 0.0002, 0.0000), Vector(0.0005, 0.0002, 0.0000), Vector(0.0003, 0.0001, 0.0000),
		Vector(0.0002, 0.0001, 0.0000), Vector(0.0002, 0.0001, 0.0000), Vector(0.0001, 0.0000, 0.0000),
		Vector(0.0001, 0.0000, 0.0000), Vector(0.0001, 0.0000, 0.0000), Vector(0.0000, 0.0000, 0.0000),
	};
}

namespace spectral {

	XYZColor getXYZResponseValue(float wavelength) {
		// Map wavelength to a discrete position in [0..80]
		float wMin = ((int)wavelength / 5) * 5.0f;
		int b0 = (int)((wMin - 380.0f) / 400.0f * (RESOLUTION - 1.0f));
		int b1 = b0 >= (RESOLUTION - 1) ? RESOLUTION - 1 : b0 + 1;
		// Do linear interpolation
		XYZColor res = cie::curves[b0] + (cie::curves[b1] - cie::curves[b0]) * (wavelength - wMin) / 5.0f;
		return res;
	}

	XYZColor toXYZ(float wavelength, float energy) {
		// Map wavelength to a discrete position in [0..80]
		float wMin = ((int)wavelength / 5) * 5.0f;
		int b0 = (int)((wMin - 380.0f) / 400.0f * (RESOLUTION - 1.0f));
		int b1 = b0 >= (RESOLUTION - 1) ? RESOLUTION - 1 : b0 + 1;
		// Do linear interpolation
		XYZColor res = cie::curves[b0] + (cie::curves[b1] - cie::curves[b0]) * (wavelength - wMin) / 5.0f;
		return res * energy;
	}

	XYZColor rgb2xyz(const Vector &rgb) {
		XYZColor xyz;
		xyz.x = 0.5767309 * rgb.x + 0.1855540 * rgb.y + 0.1881852 * rgb.z;
		xyz.y = 0.2973769 * rgb.x + 0.6273491 * rgb.y + 0.0752741 * rgb.z;
		xyz.z = 0.0270343 * rgb.x + 0.0706872 * rgb.y + 0.9911085 * rgb.z;
		return xyz;
	}

	XYZColor xyzTorgb(const XYZColor &xyzColor) {
		XYZColor rgb;
		// Bruce RGB (D65)
		rgb.x = +2.7454669f * xyzColor.x - 1.1358136f * xyzColor.y - 0.4350269f * xyzColor.z;
		rgb.y = -0.9692660f * xyzColor.x + 1.8760108f * xyzColor.y + 0.0415560f * xyzColor.z;
		rgb.z = +0.0112723f * xyzColor.x - 0.1139754f * xyzColor.y + 1.0132541f * xyzColor.z;
		return rgb;
	}

}

Spectrum createBellCurve(float wavelength, float scale) {
	Spectrum spec;
	for (int i = 0; i < WAVELENGTH_SAMPLES; ++i) {
		float x = ((float) i) * 5.0f + 380.0f - wavelength;
		spec.values[i] = 1.0f / expf(x * x * 0.05f) * scale;
	}
	return spec;
}

Spectrum Spectrum::operator+(const Spectrum &s) const {
	Spectrum spec;
	for (int i = 0; i < WAVELENGTH_SAMPLES; ++i) {
		spec.values[i] = values[i] + s.values[i];
	}
	return spec;
}

Spectrum Spectrum::operator/(float s) const {
	Spectrum spec;
	const float ss = 1.0f / s;
	for (int i = 0; i < WAVELENGTH_SAMPLES; ++i) {
		spec.values[i] = values[i] * ss;
	}
	return spec;
}

Spectrum Spectrum::operator*(float s) const {
	Spectrum spec;
	for (int i = 0; i < WAVELENGTH_SAMPLES; ++i) {
		spec.values[i] = values[i] * s;
	}
	return spec;
}

Spectrum Spectrum::operator*(const Spectrum &s) const {
	Spectrum spec;
	for (int i = 0; i < WAVELENGTH_SAMPLES; ++i) {
		spec.values[i] = values[i] * s.values[i];
	}
	return spec;
}

Vector Spectrum::toXYZ() const {
	Vector XYZ;
	for (int i = 0; i < WAVELENGTH_SAMPLES; ++i) {
		XYZ += cie::curves[i] * values[i];
	}
	return XYZ;
}

NAMESPACE_DEBRIS_END
