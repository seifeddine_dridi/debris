#include "stdafx_core.h"
#include <primitive.h>

NAMESPACE_DEBRIS_BEGIN

void Primitive::postIntersection(const RTCRay &ray, IntersectionRecord *record) const {
	DifferentialGeometry dg;
	shape->postIntersect(ray, dg);
	record->diffGeometry = dg;
	record->shader = shader;
}

const shared_ptr<AbstractShape> &Primitive::getShape() const {
	return shape;	
}

const shared_ptr<Shader> &Primitive::getShader() const {
	return shader;
}

bool Primitive::isEmitter() const {
	return shader->isEmitter();
}

NAMESPACE_DEBRIS_END
