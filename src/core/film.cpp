#include "stdafx_core.h"
#include <film.h>

#include <OpenImageIO/imageio.h>
#include <OpenImageIO/imagebufalgo.h>

OIIO_NAMESPACE_USING

NAMESPACE_DEBRIS_BEGIN

Film::Film(int width, int height) : imageWidth(width), imageHeight(height), invWidth(1.0f / imageWidth), invHeight(1.0f / imageHeight) {
	frameBuffer = new XYZColor[imageWidth * imageHeight];
}

Film::~Film() {
	if (frameBuffer) {
		delete[] frameBuffer;
	}
}

void Film::set(int x, int y, const XYZColor &c) {
	frameBuffer[x + y * imageWidth] = c;
}

void Film::add(int x, int y, const XYZColor &c) {
	frameBuffer[x + y * imageWidth] += c;
}

void Film::multiply(float scalar) {
	for (int t = 0; t < imageWidth * imageHeight; ++t) {
		frameBuffer[t] *= scalar;
	}
}

/*! Write Image */
bool Film::writeToFile(const char *filename) const {
	ImageSpec spec(imageWidth, imageHeight, 3, TypeDesc::FLOAT);
	ImageBuf imageBuf(spec);
	std::string outputFilename(filename);
	std::string imageExt;
	if (outputFilename.length() >= 4) {
		imageExt = outputFilename.substr(outputFilename.length() - 4);
	}
	if (imageExt == ".exr" || imageExt == ".hdr") {
		for (int t = 0; t < imageWidth * imageHeight; ++t) {
			Vector c = spectral::xyzTorgb(frameBuffer[t]);
			imageBuf.setpixel(t, &c[0]);
		}
	} else {
		for (int t = 0; t < imageWidth * imageHeight; ++t) {
			Vector c = spectral::xyzTorgb(frameBuffer[t]);
			// Clamp
			c.x = std::max(0.0f, std::min(c.x, 1.0f));
			c.y = std::max(0.0f, std::min(c.y, 1.0f));
			c.z = std::max(0.0f, std::min(c.z, 1.0f));
			// Gamma correction
			c.x = powf(c.x, 1.0f / 2.2f);
			c.y = powf(c.y, 1.0f / 2.2f);
			c.z = powf(c.z, 1.0f / 2.2f);
			imageBuf.setpixel(t, &c[0]);
		}
	}
	const int textPosx = 4, textPosy = 10, fontSize = 12;
	const float charWidth = 6.6f;
	const char *label = "Image rendered by Debris";
	ImageBufAlgo::zero(imageBuf, ROI(textPosx, textPosx + charWidth * strlen(label), textPosy - fontSize, textPosy + fontSize / 3));
	ImageBufAlgo::render_text(imageBuf, textPosx, textPosy, label, fontSize, "ariali");
	return imageBuf.write(filename);
}

NAMESPACE_DEBRIS_END
