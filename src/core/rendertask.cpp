#include "stdafx_core.h"
#include <rendertask.h>

#include <renderer.h>
#include <core/film.h>
#include <core/scene.h>

NAMESPACE_DEBRIS_BEGIN

RenderTask::RenderTask(Renderer *renderer, const shared_ptr<Scene> &scene, float time, int spp,
ImageSlice tile, TaskEventBroadcaster *renderStatistics) : Task(0, renderStatistics),
renderer(renderer), scene(scene), time(time), spp(spp), tile(tile) {}

void RenderTask::run() {
	tasksStatistics->taskStarted(id);
	renderer->renderFrame(scene, time, spp, tile);
	tasksStatistics->taskEnded(id);
}

NAMESPACE_DEBRIS_END
