#include "stdafx_core.h"
#include <light.h>

#include <core/primitive.h>

NAMESPACE_DEBRIS_BEGIN

Vector AreaLight::sampleArea(float u1, float u2, float &pdf) const {
	return primitive->getShape()->sampleArea(u1, u2, pdf);
}

float AreaLight::sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const {
	Vector pos = sampleArea(u1, u2, pdf);
	dir = pos - point;
	float dist = dir.length();
	dir /= dist;
	Vector shapeNormal = primitive->getShape()->getNormal(pos);
	float cosTo = std::max(0.0f, dir.dot(shapeNormal) * (-1.0f));
	if (cosTo == 0.0f) {
		pdf = 0.0f;
	} else {
		pdf = (pdf / cosTo) * dist * dist;
	}
	auto shader = primitive->getShader();
	return dist;
}

bool AreaLight::hasArea() const {
	return true;
}

bool AreaLight::isDirectional() const {
	return false;
}

XYZColor AreaLight::getEmittance(const Vector &v) const {
	auto shader = primitive->getShader();
	return XYZColor(1.0f, 1.0f, 1.0f) * shader->getEmittance();
}

NAMESPACE_DEBRIS_END
