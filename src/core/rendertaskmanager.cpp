#include "stdafx_core.h"
#include <rendertaskmanager.h>

#include <rendertask.h>

NAMESPACE_DEBRIS_BEGIN

#define BLOCK_SIZE 64

RenderTaskManager::RenderTaskManager(unsigned nbThreads, shared_ptr<Renderer> &renderer,
	const shared_ptr<Scene> &scene, int spp, int imageWidth, int imageHeight) : TaskManager(nbThreads) {
	ImageSlicer *tiles = new LinearImageTileOrdering(BLOCK_SIZE, imageWidth, imageHeight);

	while (tiles->hasNext()) {
		ImageSlice tile = tiles->getNext();
		this->postTask(new RenderTask(renderer.get(), scene, 0, spp, tile, &tasksStatistics));
	}

	delete tiles;
}

RenderTaskManager::~RenderTaskManager() {

}

NAMESPACE_DEBRIS_END
