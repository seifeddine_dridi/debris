#include "stdafx_core.h"
#include <shader.h>

NAMESPACE_DEBRIS_BEGIN

XYZColor Shader::getColor(float u, float v) const {
	return texture->getColor(u, v);
}

bool Shader::isEmitter() const {
	return emittance != 0.0f;
}
float Shader::getEmittance() const {
	return emittance;
}
XYZColor Shader::getEmittedSpectrum(const DifferentialGeometry &dGeom) const {
	return texture->getColor(dGeom.uv[0], dGeom.uv[1]) * emittance;
}

NAMESPACE_DEBRIS_END
