#include "stdafx_core.h"
#include <panoramiccamera.h>

NAMESPACE_DEBRIS_BEGIN

PanoramicCamera::PanoramicCamera(const Transform &loca2world, const shared_ptr<Film> &film,
	float fieldOfView, float focalDistance) : Camera(loca2world, film), fieldOfView(fieldOfView), focalDistance(focalDistance) {
}

Ray PanoramicCamera::project(float x, float y, float u1, float u2, float t) const {
	Vector a = screen2Raster * Vector(x, y);
	float phi = atan2(a.y, a.x);
	//float r = x / cosf(phi);
	float r = sqrtf(a.lengthSquared());
	float theta = r * fieldOfView;
	Vector d(sinf(theta) * cosf(phi), sinf(theta) * sinf(phi), cosf(theta));
	Ray ray(Vector(), d, INFINITY, computeTime(t));
	return local2world * ray;
}

NAMESPACE_DEBRIS_END
