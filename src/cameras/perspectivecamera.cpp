#include "stdafx_core.h"
#include <perspectivecamera.h>

NAMESPACE_DEBRIS_BEGIN

PerspectiveCamera::PerspectiveCamera(const Transform &loca2world, const shared_ptr<Film> &film,
		float lensRadius, float focalDistance, float focusDistance) : Camera(loca2world, film),
		lensRadius(lensRadius), focalDistance(focalDistance), focusDistance(focusDistance) {
}

void ToUnitDisk(float u1, float u2, float &x, float &y) {
	float phi, r;
	float a = 2 * u1 - 1;
	float b = 2 * u2 - 1;
	if (a*a > b*b) { // Use squares instead of absolute values
		r = a;
		phi = (M_PI / 4)*(b / a);
	} else {
		r = b;
		//phi = (M_PI / 4)*(a / b) + (M_PI / 2);
		phi = (M_PI / 2) - (M_PI / 4) * (a / b);
	}
	x = r * cos(phi);
	y = r * sin(phi);
}

Ray PerspectiveCamera::project(float x, float y, float u1, float u2, float t) const {
	Vector rp = screen2Raster * Vector(x, y);
	Ray ray(Vector(), Vector(rp.x, rp.y, focalDistance), INFINITY, t);
	if (lensRadius > 0.0f) {
		float lensX, lensY;
		ToUnitDisk(u1, u2, lensX, lensY);
		lensX *= lensRadius;
		lensY *= lensRadius;
		float ft = focusDistance / ray.d.z;
		Vector focusPoint = ray(ft);
		ray.o = Vector(lensX, lensY, 0.f);
		ray.d = (focusPoint - ray.o).normalize();
	}
	return local2world * ray;
}

NAMESPACE_DEBRIS_END
