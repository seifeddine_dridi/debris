#include "stdafx_core.h"
#include <orthographiccamera.h>

NAMESPACE_DEBRIS_BEGIN

OrthographicCamera::OrthographicCamera(const Transform &loca2world, const shared_ptr<Film> &film, float zoom) : Camera(loca2world, film), zoom(zoom) {
}

Ray OrthographicCamera::project(float x, float y, float u1, float u2, float t) const {
	Vector a = screen2Raster * Vector(x, y);
	Vector d(0, 0, 1.0f);
	Ray ray(Vector(a.x, a.y, 0) * zoom, d, INFINITY, computeTime(t));
	return local2world * ray;
}

NAMESPACE_DEBRIS_END
