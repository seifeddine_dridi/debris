#include <gtest/gtest.h>

#include <bsdfs/specularreflector.h>

using namespace debris;

TEST(BsdfTest, SpecularReflectionTest) {
	SpecularReflector spec(XYZColor(), 1.0f);
	Vector wi;
	Vector wo = Vector(0.5f, -0.0f).normalize();
	spec.sample(Random(), wi, wo, Vector(0, 1, 0));
}