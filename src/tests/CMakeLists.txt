set(GTEST_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/../dependencies/gtest-1.7.0/include)
set(GTEST_LIBRARY_RELEASE ${PROJECT_SOURCE_DIR}/../dependencies/gtest-1.7.0/lib/x86/Release/gtest.lib)
set(GTEST_LIBRARY_DEBUG ${PROJECT_SOURCE_DIR}/../dependencies/gtest-1.7.0/lib/x86/Debug/gtest.lib)

add_library(gtest SHARED IMPORTED)
set_target_properties(gtest PROPERTIES IMPORTED_IMPLIB_RELEASE ${GTEST_LIBRARY_RELEASE} IMPORTED_IMPLIB_DEBUG ${GTEST_LIBRARY_DEBUG})

include_directories(${GTEST_INCLUDE_DIR})
# See (https://code.google.com/p/googletest/wiki/FAQ#I_am_building_my_project_with_Google_Test_in_Visual_Studio_and_a)
SET(CMAKE_CXX_FLAGS_DEBUG "/MTd")
SET(CMAKE_CXX_FLAGS_RELEASE "/MT")
file(GLOB_RECURSE src "*.h" "*.cpp")
add_executable(UnitTests ${src})
target_link_libraries(UnitTests DebrisCore gtest)
