#include <shapes/sphere.h>
#include <shapes/triangle.h>
#include <core/random.h>

#include <gtest/gtest.h>

using namespace debris;

TEST(ShapeTest, Sphere) {
	Transform4f transform(Eigen::Translation3f(0, 0, -2.0f));
	Transform local2world(transform);
	Sphere sphere(Vector(), 1.0f, local2world);
	Ray ray(Vector(), Vector(0, 0, -1.0f));
	DifferentialGeometry dg;
	ASSERT_EQ(sphere.AbstractShape::intersect(ray, dg), true);
	ASSERT_FLOAT_EQ(dg.distance, 1.0f);
	ASSERT_FLOAT_EQ(dg.pos.x, 0.0f);
	ASSERT_FLOAT_EQ(dg.pos.y, 0.0f);
	ASSERT_FLOAT_EQ(dg.pos.z, -1.0f);

	ASSERT_FLOAT_EQ(dg.normal.x, 0.0f);
	ASSERT_FLOAT_EQ(dg.normal.y, 0.0f);
	ASSERT_FLOAT_EQ(dg.normal.z, 1.0f);
}

TEST(ShapeTest, RaySphereIntersection) {
	Sphere sphere(Vector(), 100.0f, Transform());
	const int nbSamples = 10;
	Random rng;
	for (int i = 0; i < nbSamples; ++i) {
		Ray ray(Vector(), Vector(rng.nextFloat(), rng.nextFloat(), rng.nextFloat()));
		if (!sphere.AbstractShape::intersectP(ray)) {
			GTEST_FAIL();
		}
	}
}

//TEST(ShapeTest, Triangle) {
//	Vector vertices[] = { Vector(), Vector(1), Vector(0, 1) };
//	Transform4f transform(Eigen::Translation3f(0, 0, 0));
//	Transform local2world(transform);
//	Triangle triangle(local2world, vertices);
//	Ray ray(Vector(0.125f,0.25f, 1.0f), Vector(0, 0, -1.0f));
//	DifferentialGeometry dg;
//	ASSERT_EQ(triangle.AbstractShape::intersect(ray, dg), true);
//	ASSERT_FLOAT_EQ(dg.distance, 1.0f);
//
//	ASSERT_FLOAT_EQ(dg.normal.x, 0.0f);
//	ASSERT_FLOAT_EQ(dg.normal.y, 0.0f);
//	ASSERT_FLOAT_EQ(dg.normal.z, 1.0f);
//}
