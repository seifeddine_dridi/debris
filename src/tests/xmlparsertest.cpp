#include <parsers/xml/scenehandler.h>

#include <gtest/gtest.h>

using namespace xercesc;
using namespace debris;

TEST(XmlParserTest, TestScene) {
	try {
		XMLPlatformUtils::Initialize();
		SAX2XMLReader *xmlReader = XMLReaderFactory::createXMLReader();
		xmlReader->setFeature(XMLUni::fgSAX2CoreValidation, true);
		xmlReader->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);
		xmlReader->setFeature(XMLUni::fgXercesSchema, true);
		xmlReader->setFeature(XMLUni::fgXercesSchemaFullChecking, true);
		xmlReader->setFeature(XMLUni::fgXercesValidationErrorAsFatal, true);
		xmlReader->setFeature(XMLUni::fgXercesUseCachedGrammarInParse, true);
		xmlReader->setFeature(XMLUni::fgXercesLoadSchema, true);
		xmlReader->setValidationConstraintFatal(true);
		xmlReader->loadGrammar("../../data/schema/Debris.xsd", Grammar::SchemaGrammarType, true);
		SceneHandler sceneHandler;
		xmlReader->setContentHandler(&sceneHandler);
		xmlReader->setErrorHandler(&sceneHandler);
		xmlReader->parse("../../src/tests/SampleScene.xml");
		std::cout << "XML file is valid" << std::endl;

		SceneNode *sceneNode = sceneHandler.getSceneNode();
		ASSERT_EQ(sceneNode->primitives.size(), 1);
		ASSERT_EQ(sceneNode->lights.size(), 1);
		ASSERT_TRUE(sceneNode->camera.use_count() >= 1);
		const Transform &cameraTransform = sceneNode->camera->getTransform();
		Vector camPos = cameraTransform * Vector();
		ASSERT_EQ((int)camPos.x, 0);
		ASSERT_EQ((int)camPos.y, 1);
		ASSERT_EQ((int)camPos.z, -10);

		delete xmlReader;
		XMLPlatformUtils::Terminate();
	} catch (const XMLException& ex) {
		std::cout << ex.getMessage() << std::endl;
	}
}