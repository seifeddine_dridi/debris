#include <core/geometry/vector.h>

#include <gtest/gtest.h>

using namespace debris;

TEST(VectorTest, Init) {
	Vector v(1, 2, 3);
	ASSERT_FLOAT_EQ(v.x, 1.0f);
	ASSERT_FLOAT_EQ(v.y, 2.0f);
	ASSERT_FLOAT_EQ(v.z, 3.0f);

	Vector v1 = 5;
	ASSERT_FLOAT_EQ(v1.x, 5.0f);
	ASSERT_FLOAT_EQ(v1.y, 0.0f);
	ASSERT_FLOAT_EQ(v1.z, 0.0f);

	Vector v2 = v + v1;
	ASSERT_FLOAT_EQ(v2.x, 6.0f);
	ASSERT_FLOAT_EQ(v2.y, 2.0f);
	ASSERT_FLOAT_EQ(v2.z, 3.0f);
}

TEST(VectorTest, CrossProduct) {
	Vector a(1.0f), b(0, 1.0f);
	Vector c = a % b;
	ASSERT_FLOAT_EQ(c.x, 0.0f);
	ASSERT_FLOAT_EQ(c.y, 0.0f);
	ASSERT_FLOAT_EQ(c.z, 1.0f);

	Vector res = Vector(0, 0, -1.0f) % Vector(0, 1.0f);
	ASSERT_FLOAT_EQ(res.x, 1.0f);
	ASSERT_FLOAT_EQ(res.y, 0.0f);
	ASSERT_FLOAT_EQ(res.z, 0.0f);
}

TEST(VectorTest, Division) {
	Vector v(10.0f, 8.0f, 6.0f);
	v /= 2.0f;
	ASSERT_FLOAT_EQ(v.x, 5.0f);
	ASSERT_FLOAT_EQ(v.y, 4.0f);
	ASSERT_FLOAT_EQ(v.z, 3.0f);
}