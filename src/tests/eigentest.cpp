#include <Eigen/Geometry>
#include <Eigen/src/Core/util/Constants.h>
#include <gtest/gtest.h>

TEST(EigenTest, Matrix22Inversion) {
	Eigen::Matrix2f matrix;
	matrix(0, 0) = 5.0f;
	matrix(0, 1) = 0.0f;
	matrix(1, 1) = 5.0f;
	matrix(1, 0) = 0.0f;
	Eigen::Matrix2f inverseMatrix = matrix.inverse();
	Eigen::Matrix2f result = matrix * inverseMatrix;
	ASSERT_FLOAT_EQ(result(0, 0), 1.0f);
	ASSERT_FLOAT_EQ(result(0, 1), 0.0f);
	ASSERT_FLOAT_EQ(result(1, 0), 0.0f);
	ASSERT_FLOAT_EQ(result(1, 1), 1.0f);
}

TEST(EigenTest, SpaceTransform) {
	Eigen::Transform<float, 3, Eigen::TransformTraits::Affine> transform = Eigen::Translation3f(0, 1.0f, 1.0f) * Eigen::Scaling(2.0f, 2.0f, 2.0f);
	Eigen::Transform<float, 3, Eigen::TransformTraits::Affine> inverseTransform = transform.inverse();
	Eigen::Vector3f p(0, 1.0f, 1.0f);
	Eigen::Vector3f tPoint = inverseTransform * (transform * p);
	ASSERT_FLOAT_EQ(tPoint[0], 0.0f);
	ASSERT_FLOAT_EQ(tPoint[1], 1.0f);
	ASSERT_FLOAT_EQ(tPoint[2], 1.0f);
}