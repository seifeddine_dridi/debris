#include <core/propertylist.h>

#include <gtest/gtest.h>

using namespace debris;

TEST(PropertyListTest, PropsListBasicOps) {
	PropertyList pList;
	pList.addInt("prop1", 5);
	ASSERT_FLOAT_EQ(pList.getInt("prop1"), 5);

	pList.addFloat("prop2", 3.14f);
	ASSERT_FLOAT_EQ(pList.getFloat("prop2"), 3.14f);
}