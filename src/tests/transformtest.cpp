#include <core/geometry/transform.h>
#include <core/random.h>
#include <gtest/gtest.h>

using namespace debris;

void test(float x, float y, float z) {
 	Transform t(Vector(x, y, z));
	Transform4f matrix = t.getMatrix();
	Vector u(matrix(0, 0), matrix(1, 0), matrix(2, 0));
	Vector v(matrix(0, 1), matrix(1, 1), matrix(2, 1));
	Vector w(matrix(0, 2), matrix(1, 2), matrix(2, 2));
	EXPECT_NEAR(0.0f, u.dot(v), EPSILON);
	EXPECT_NEAR(0.0f, u.dot(w), EPSILON);
	EXPECT_NEAR(0.0f, v.dot(w), EPSILON);
}

TEST(TransformTest, OrthogonalBasisGeneration) {
	test(0, 0, 1.0f);
	test(0, 1.0f, 0);
	test(1.0f, 0, 0);
	Random rng;
	for (int i = 0; i < 100; ++i) {
		float z = 1.0f - 2.0f * rng.nextFloat();
		float sintheta = sqrtf(1.0f - z * z);
		float phi = rng.nextFloat() * 2.0f * M_PI;
		float x = cosf(phi) * sintheta;
		float y = sinf(phi) * sintheta;
		test(x, y, z);
	}
}