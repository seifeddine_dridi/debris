#include <core/spectrum.h>

#include <gtest/gtest.h>

using namespace debris;

TEST(SpectrumTest, BasicOps) {
	Spectrum spec1(3.0f);
	Spectrum spec2(2.0f);
	Spectrum res = spec1 * spec2;
	ASSERT_FLOAT_EQ(res.values[0], 6.0f);

	Spectrum spec3 = createBellCurve(600.0f, 1.0f);
}