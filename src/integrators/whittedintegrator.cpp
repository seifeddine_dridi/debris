#include "stdafx_core.h"
#include <whittedintegrator.h>

#include <boost/foreach.hpp>

#include <core/scene.h>

NAMESPACE_DEBRIS_BEGIN

XYZColor WhittedIntegrator::getRadiance(const Ray &ray, const shared_ptr<Scene> &scene, float wavelength, Random &rng) const {
	return getRadiance(ray, scene, wavelength, 1, rng);
}

XYZColor WhittedIntegrator::getRadiance(const Ray &ray, const shared_ptr<Scene> &scene, float wavelength, int depth, Random &rng) const {
	if (depth >= m_maxBounces) {
		return scene->getBackgroundRadiance(ray);
	}
	IntersectionRecord info;
	if (!scene->intersect(ray, &info)) {
		return scene->getBackgroundRadiance(ray);
	}
	if (info.shader->isEmitter()) {
		return info.shader->getEmittedSpectrum(info.diffGeometry);
	}
	BSDFContainer bsdfContainer;
	info.shader->createBSDF(info.diffGeometry, wavelength, bsdfContainer);
	BSDF *bsdf = bsdfContainer.getBSDF();
	if (bsdf->getType() == ReflectionType::SPECULAR) {
		Vector wi;
		XYZColor f = bsdf->sample(rng, wi, ray.d, info.diffGeometry.normal);
		return f * getRadiance(Ray(info.diffGeometry.pos + wi * EPSILON, wi, INFINITY, ray.time), scene, wavelength, depth + 1, rng);
	} else if (bsdf->getType() == ReflectionType::DIFFUSE) {
		return computeDirectLighting(ray, info, scene, wavelength, rng);
	} else {
		return 0.0f;
	}
}

XYZColor WhittedIntegrator::computeDirectLighting(const Ray &ray, const IntersectionRecord &info,
	const shared_ptr<Scene> &scene, float wavelength, Random &rng) const {
	XYZColor directRadiance;
	const std::vector<shared_ptr<Light>> &lights = scene->getLights();
	BOOST_FOREACH(shared_ptr<Light> light, lights) {
		float pdf;
		Vector lightDir;
		float dist = light->sampleLight(info.diffGeometry.pos, info.diffGeometry.normal, rng.nextFloat(), rng.nextFloat(), pdf, lightDir);
		if (pdf == 0.0f) {
			continue;
		}
		Ray shadowRay(info.diffGeometry.pos, lightDir, dist - EPSILON, ray.time);
		if (!scene->intersectP(shadowRay)) {
			float cosTi = std::max(0.0f, lightDir.dot(info.diffGeometry.normal));
			BSDFContainer bsdfContainer;
			info.shader->createBSDF(info.diffGeometry, wavelength, bsdfContainer);
			BSDF *bsdf = bsdfContainer.getBSDF();
			Vector wo = ray.d;
			wo.normalize();
			directRadiance += bsdf->eval(wo, lightDir, info.diffGeometry.normal) * light->getEmittance(lightDir) * cosTi / pdf;
		}
	}
	return directRadiance / lights.size();
}

NAMESPACE_DEBRIS_END
