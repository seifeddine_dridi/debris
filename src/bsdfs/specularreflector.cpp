#include "stdafx_core.h"
#include <specularreflector.h>

NAMESPACE_DEBRIS_BEGIN

XYZColor SpecularReflector::sample(Random &rng, Vector &wi, const Vector &wo, const Vector &normal) const {
	//Transform local2world(normal);
	//Transform world2local(local2world.getMatrix().inverse());
	//Vector woLocal = world2local * (-wo);
	//Vector wiLocal(-woLocal.x, -woLocal.y, woLocal.z);
	wi = wo + normal * (-2.0f) * wo.dot(normal);
	//wi = local2world * wiLocal;
	return spectrum * kr;
}

XYZColor SpecularReflector::eval(const Vector &wi, const Vector &wo, const Vector &normal) const {
	return spectrum * kr;
}

float SpecularReflector::getPdf(const Vector &wi, const Vector &wo, const Vector &normal) const {
	return 0;
}

NAMESPACE_DEBRIS_END
