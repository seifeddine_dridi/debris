#include "stdafx_core.h"
#include <lambert.h>

NAMESPACE_DEBRIS_BEGIN

XYZColor Lambert::sample(Random &rng, Vector &wi, const Vector &wo, const Vector &normal) const {
	float u1 = rng.nextFloat(), u2 = rng.nextFloat();
	float sintheta = sqrtf(u1), costheta = sqrtf(1.0f - u1);
	Vector dir(cosf(2.0f * (float) M_PI * u2) * sintheta, sinf(2.0f * (float) M_PI * u2) * sintheta, costheta);
	// Construct an affine space
	AffineSpace affineSpace(normal);
	wi = affineSpace(dir);
	// Return the ratio of brdf over pdf
	// Integration is done wrt. projected solid angle
	return spectrum * kd;
}

XYZColor Lambert::eval(const Vector &wi, const Vector &wo, const Vector &normal) const {
	return spectrum * kd / (float) M_PI;
}

float Lambert::getPdf(const Vector &wi, const Vector &wo, const Vector &normal) const {
	return std::max(wi.dot(normal), 0.0f) / (float) M_PI;
}

NAMESPACE_DEBRIS_END
