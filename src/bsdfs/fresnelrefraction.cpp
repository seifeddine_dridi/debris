#include "stdafx_core.h"
#include <fresnelrefraction.h>

NAMESPACE_DEBRIS_BEGIN

float computeFresnelReflection(float iorOut, float iorIn, float cosTi, float cosTo) {
	float r1 = (iorOut * cosTi - iorIn * cosTo) / (iorOut * cosTi + iorIn * cosTo);
	float r2 = (iorIn * cosTi - iorOut * cosTo) / (iorIn * cosTi + iorOut * cosTo);
	return (r1 * r1 + r2 * r2) * 0.5f;
}

XYZColor FresnelRefraction::sample(Random &rng, Vector &wi, const Vector &wo, const Vector &normal) const {
	float cosTi = normal.dot(wo);
	float etaRatio;
	Vector n;
	float fresnelReflection;
	float eta1, eta2;
	if (cosTi < 0) {
		// Ray is coming from inside
		cosTi *= -1.0f;
		etaRatio = iorOut / iorIn;
		n = normal;
		eta1 = iorIn;
		eta2 = iorOut;
	} else {
		etaRatio = iorIn / iorOut;
		n = -normal;
		eta1 = iorOut;
		eta2 = iorIn;
	}
	float sinTo2 = etaRatio * etaRatio * (1.0f - cosTi * cosTi);
	if (sinTo2 > 1.0f) {
		// Total internal reflection
		wi = wo - n * 2.0f * n.dot(wo);
		return spectrum;
	}
	wi = wo * etaRatio + n * (etaRatio * cosTi - sqrtf(1.0f - sinTo2));
	// Compute coefficient of refraction
	fresnelReflection = computeFresnelReflection(eta1, eta2, cosTi, sqrtf(1.0f - sinTo2));
	return spectrum * (1.0f - fresnelReflection);
}

XYZColor FresnelRefraction::eval(const Vector &wi, const Vector &wo, const Vector &normal) const {
	return XYZColor();
}

float FresnelRefraction::getPdf(const Vector &wi, const Vector &wo, const Vector &normal) const {
	return 0;
}

NAMESPACE_DEBRIS_END
