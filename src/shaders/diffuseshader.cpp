#include "stdafx_core.h"
#include <diffuseshader.h>

#include <bsdfs/lambert.h>

NAMESPACE_DEBRIS_BEGIN

DiffuseShader::DiffuseShader(const shared_ptr<Texture> &texture, float kd, float emittance) : Shader(texture, emittance), kd(kd) {
}

void DiffuseShader::createBSDF(const DifferentialGeometry &dGeom, float wavelength, BSDFContainer &bsdfContainer) const {
	bsdfContainer.setBSDF(NEW_BSDF(Lambert, texture->getColor(dGeom.uv[0], dGeom.uv[1]), kd));
}

NAMESPACE_DEBRIS_END
