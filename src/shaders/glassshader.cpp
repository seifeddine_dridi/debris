#include "stdafx_core.h"
#include <glassshader.h>

#include <bsdfs/fresnelrefraction.h>

NAMESPACE_DEBRIS_BEGIN

GlassShader::GlassShader(const shared_ptr<Texture> &texture, float iorOut, float iorIn) : Shader(texture, 0.0f),
	iorOut(iorOut), iorIn(iorIn) {
}

void GlassShader::createBSDF(const DifferentialGeometry &dGeom, float wavelength, BSDFContainer &bsdfContainer) const {
	bsdfContainer.setBSDF(NEW_BSDF(FresnelRefraction, texture->getColor(dGeom.uv[0], dGeom.uv[1]), iorOut, iorIn));
}

NAMESPACE_DEBRIS_END
