#include "stdafx_core.h"
#include <mirrorshader.h>

#include <bsdfs/specularreflector.h>

NAMESPACE_DEBRIS_BEGIN

MirrorShader::MirrorShader(const shared_ptr<Texture> &texture, float kr, float emittance) : Shader(texture, emittance), kr(kr) {
}

void MirrorShader::createBSDF(const DifferentialGeometry &dGeom, float wavelength, BSDFContainer &bsdfContainer) const {
	bsdfContainer.setBSDF(NEW_BSDF(SpecularReflector, texture->getColor(dGeom.uv[0], dGeom.uv[1]), kr));
}

NAMESPACE_DEBRIS_END
