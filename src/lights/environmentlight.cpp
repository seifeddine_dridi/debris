#include "stdafx_core.h"
#include <environmentlight.h>

#include <core/geometry/transform.h>

NAMESPACE_DEBRIS_BEGIN

inline Vector cosineSampleUnitHemisphere(float u1, float u2) {
	float sintheta = sqrtf(u1);
	float costheta = sqrtf(1.0f - u1);
	float phi = 2.0f * M_PI * u2;
	return Vector(cosf(phi) * sintheta, sinf(phi) * sintheta, costheta);
}

inline float cosineSampleUnitHemisphere(const Vector &w) {
	return w.z / M_PI;
}

inline Vector sampleUnitSphere(float u1, float u2) {
	float z = 1.0f - 2.0f * u2;
	float sintheta = sqrtf(1.0f - z * z);
	float phi = u1 * 2.0f * M_PI;
	float x = cosf(phi) * sintheta;
	float y = sinf(phi) * sintheta;
	return Vector(x, y, z);
}

EnvironmentLight::EnvironmentLight(const char *imagePath) : hdrImage(imagePath) {}

float EnvironmentLight::sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const {
	// Sample unit sphere area
	//Vector sample = sampleUnitSphere(u1, u2);
	Vector sample = cosineSampleUnitHemisphere(u1, u2);
	dir = Transform(normal) * sample;
	//dir = sample;
	pdf = cosineSampleUnitHemisphere(sample);
	//pdf = 1.0f / (4.0f * M_PI);
	// Return the distance to the light source
	return INFINITY;
}

bool EnvironmentLight::hasArea() const {
	return true;
}

bool EnvironmentLight::isDirectional() const {
	return false;
}

XYZColor EnvironmentLight::getEmittance(const Vector &v) const {
	Vector nv(v.x, v.z, v.y);
	float phi = atan2(nv.y, nv.x);
	if (phi < 0.0f) {
		phi += 2.0f * (float) M_PI;
	}
	float uv[2];
	uv[0] = phi / (2.0f * M_PI);
	uv[1] = (1.0f - nv.z) / 2.0f;
	uv[0] = std::max(0.0f, std::min(uv[0], 1.0f));
	uv[1] = std::max(0.0f, std::min(uv[1], 1.0f));
	return hdrImage.getColor(uv[0], uv[1]);
}

NAMESPACE_DEBRIS_END
