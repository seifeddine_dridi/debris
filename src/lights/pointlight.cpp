#include "stdafx_core.h"
#include <pointlight.h>

NAMESPACE_DEBRIS_BEGIN

PointLight::PointLight(const Vector &position, float intensity) {
	this->position = position;
	this->intensity = intensity;
}

Vector PointLight::sampleArea(float u1, float u2, float &pdf) const {
	pdf = 1.0f;
	return position;
}

float PointLight::sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const {
	pdf = 1.0f;
	dir = position - point;
	float dist = dir.length();
	dir /= dist;
	pdf = dist * dist;
	return dist;
}

bool PointLight::hasArea() const {
	return false;
}

bool PointLight::isDirectional() const {
	return false;
}

XYZColor PointLight::getEmittance(const Vector &v) const {
	return XYZColor(0.8f, 0.8f, 0.8f) * intensity;
}

NAMESPACE_DEBRIS_END
