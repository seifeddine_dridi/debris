#include "stdafx_core.h"
#include <directionallight.h>

NAMESPACE_DEBRIS_BEGIN

DirectionalLight::DirectionalLight(const Vector &dir, float radiance) {
	this->dir = dir;
	this->radiance = radiance;
}

Vector DirectionalLight::sampleArea(float u1, float u2, float &pdf) const {
	throw DebrisException("Directional light has no area to sample");
}

float DirectionalLight::sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const {
	pdf = 1.0f;
	dir = -this->dir;
	return INFINITY;
}

bool DirectionalLight::hasArea() const {
	return false;
}

bool DirectionalLight::isDirectional() const {
	return true;
}

XYZColor DirectionalLight::getEmittance(const Vector &v) const {
	return XYZColor(0.8f, 0.8f, 0.8f) * radiance;
}

NAMESPACE_DEBRIS_END
