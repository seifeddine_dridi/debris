#include "stdafx_core.h"
#include <colorfunction.h>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

NAMESPACE_DEBRIS_BEGIN

void ColorFunction::parse(const std::string &colorStr, float *params, int N) {
	std::string functionName = getName();
	std::string rgb = colorStr.substr(functionName.size() + 1, colorStr.size() - functionName.size() - 2);
	boost::tokenizer<boost::char_separator<char>> tokens(rgb, boost::char_separator<char>(","));
	int count = 0;
	BOOST_FOREACH(std::string token, tokens) {
		if (count >= N) {
			break;
		}
		try {
			params[count] = boost::lexical_cast<float>(token);
		}
		catch (const bad_lexical_cast &ex) {
			std::cout << ex.what() << std::endl;
		}
		++count;
	}
}

NAMESPACE_DEBRIS_END
