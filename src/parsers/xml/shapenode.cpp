#include "stdafx_core.h"
#include <shapenode.h>

#include <scenenode.h>

NAMESPACE_DEBRIS_BEGIN

Scene *ShapeNode::getScene() const {
	return ((SceneNode*)parent)->scene.get();
}

NAMESPACE_DEBRIS_END
