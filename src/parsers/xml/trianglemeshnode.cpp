#include "stdafx_core.h"
#include <trianglemeshnode.h>

#include "rply-1.1.3/rply.h"

NAMESPACE_DEBRIS_BEGIN

struct TrMesh {
	TrMesh() : currentVertexIndex(0), currentTriangleIndex(0), currentVertexPropIndex(0), uvs(nullptr) {}
	~TrMesh() {
		delete[] vertices;
		delete[] uvs;
		delete[] faces;
	}

	Vector *vertices;
	float *uvs;
	int *faces;
	int currentVertexIndex;
	int currentVertexPropIndex;
	int currentTriangleIndex;
};

static int vertex_cb(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	TrMesh *trMesh;
	ply_get_argument_user_data(argument, (void **)&trMesh, NULL);
	trMesh->vertices[trMesh->currentVertexIndex][trMesh->currentVertexPropIndex++] = (float) ply_get_argument_value(argument);
	if (eol) {
		trMesh->currentVertexIndex++;
		trMesh->currentVertexPropIndex = 0;
	}
	return 1;
}

static int vertex_uv_cb(p_ply_argument argument) {
	static int currentUVIndex = 0;
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	TrMesh *trMesh;
	ply_get_argument_user_data(argument, (void **)&trMesh, NULL);
	trMesh->uvs[currentUVIndex++] = (float)ply_get_argument_value(argument);
	return 1;
}

static int face_cb(p_ply_argument argument) {
	long length, value_index;
	ply_get_argument_property(argument, NULL, &length, &value_index);
	TrMesh *trMesh;
	ply_get_argument_user_data(argument, (void **)&trMesh, NULL);
	int fIndex = (int) ply_get_argument_value(argument);
	switch (value_index) {
	case 0:
	case 1:
		trMesh->faces[trMesh->currentTriangleIndex++] = fIndex;
		break;
	case 2:
		trMesh->faces[trMesh->currentTriangleIndex++] = fIndex;
		break;
	default:
		break;
	}
	return 1;
}

void TriangleMeshNode::createShape() {
	if (!shader.use_count()) {
		throw new DebrisException("shader element is missing from <triangle-mesh>");
	}
	p_ply ply = ply_open(src.c_str(), NULL, 0, NULL);
	if (!ply) {
		throw DebrisException(std::string("Could not read PLY file: ") + src);
	}
	if (!ply_read_header(ply)) {
		throw DebrisException("PLY file header is corrupted or missing");
	}
	TrMesh trMesh;
	long nbVertices = ply_set_read_cb(ply, "vertex", "x", vertex_cb, &trMesh, 0);
	ply_set_read_cb(ply, "vertex", "y", vertex_cb, &trMesh, 0);
	ply_set_read_cb(ply, "vertex", "z", vertex_cb, &trMesh, 1);
	/* UV coordinates callback functions */
	long nbUVs = ply_set_read_cb(ply, "vertex", "s", vertex_uv_cb, &trMesh, 0);
	ply_set_read_cb(ply, "vertex", "t", vertex_uv_cb, &trMesh, 0);
	if (nbUVs) {
		trMesh.uvs = new float[nbUVs * 2];
	}
	long nbTriangles = ply_set_read_cb(ply, "face", "vertex_indices", face_cb, &trMesh, 0);
	trMesh.vertices = new Vector[nbVertices];
	trMesh.faces = new int[nbTriangles * 3];
	if (!ply_read(ply)) {
		throw DebrisException("PLY file is invalid");
	}
	ply_close(ply);
	shape = boost::shared_ptr<AbstractShape>(new TriangleMesh(transform, trMesh.vertices, trMesh.faces, trMesh.uvs, nbVertices, nbTriangles));
}

NAMESPACE_DEBRIS_END
