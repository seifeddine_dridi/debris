#include "stdafx_core.h"
#include <perspectivecameranode.h>

#include <cameras/perspectivecamera.h>

NAMESPACE_DEBRIS_BEGIN

Camera *PerspectiveCameraNode::instantiateCamera() const {
	return new PerspectiveCamera(transform, shared_ptr<Film>(new Film(width, height)),
		lensRadius, focalDistance, focusDistance);
}

NAMESPACE_DEBRIS_END
