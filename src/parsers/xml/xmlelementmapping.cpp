#include "stdafx_core.h"
#include <xmlelementmapping.h>

#include <scenenode.h>
#include <spherenode.h>
#include <trianglenode.h>
#include <trianglemeshnode.h>
#include <diffuseshadernode.h>
#include <mirrorshadernode.h>
#include <glassshadernode.h>
#include <perspectivecameranode.h>
#include <orthographiccameranode.h>
#include <panoramiccameranode.h>
#include <pointlightnode.h>
#include <directionallightnode.h>
#include <environmentlightnode.h>

NAMESPACE_DEBRIS_BEGIN

class SceneNodeMaker : public NodeMaker {
public:
	Node *createNode(Node *parent) const {
		return new SceneNode();
	}
};

template <class T>
class GenericNodeMaker : public NodeMaker {
public:
	Node *createNode(Node *parent) const {
		return new T(parent);
	}
};

XMLElementMapping::XMLElementMapping() {
	elementMapping.insert(std::make_pair(L"scene", new SceneNodeMaker()));
	// Shapes
	elementMapping.insert(std::make_pair(L"sphere", new GenericNodeMaker<SphereNode>()));
	elementMapping.insert(std::make_pair(L"triangle", new GenericNodeMaker<TriangleNode>()));
	elementMapping.insert(std::make_pair(L"triangle-mesh", new GenericNodeMaker<TriangleMeshNode>()));
	// Textures
	elementMapping.insert(std::make_pair(L"texture", new GenericNodeMaker<TextureNode>()));
	// Shaders
	elementMapping.insert(std::make_pair(L"diffuse-shader", new GenericNodeMaker<DiffuseShaderNode>()));
	elementMapping.insert(std::make_pair(L"mirror-shader", new GenericNodeMaker<MirrorShaderNode>()));
	elementMapping.insert(std::make_pair(L"glass-shader", new GenericNodeMaker<GlassShaderNode>()));
	// Transform
	elementMapping.insert(std::make_pair(L"animated-transform", new GenericNodeMaker<AnimatedTransformNode>()));
	elementMapping.insert(std::make_pair(L"transform", new GenericNodeMaker<TransformNode>()));
	elementMapping.insert(std::make_pair(L"rotate", new GenericNodeMaker<RotateNode>()));
	elementMapping.insert(std::make_pair(L"translate", new GenericNodeMaker<TranslateNode>()));
	elementMapping.insert(std::make_pair(L"lookat", new GenericNodeMaker<LookAtNode>()));
	// Cameras
	elementMapping.insert(std::make_pair(L"perspective-camera", new GenericNodeMaker<PerspectiveCameraNode>()));
	elementMapping.insert(std::make_pair(L"orthographic-camera", new GenericNodeMaker<OrthographicCameraNode>()));
	elementMapping.insert(std::make_pair(L"panoramic-camera", new GenericNodeMaker<PanoramicCameraNode>()));
	// Lights
	elementMapping.insert(std::make_pair(L"point-light", new GenericNodeMaker<PointLightNode>()));
	elementMapping.insert(std::make_pair(L"directional-light", new GenericNodeMaker<DirectionalLightNode>()));
	elementMapping.insert(std::make_pair(L"environment-light", new GenericNodeMaker<EnvironmentLightNode>()));
}

Node *XMLElementMapping::createNode(const std::wstring &name, Node *parent) {
	std::map<const std::wstring, const NodeMaker*>::const_iterator iter = elementMapping.find(name);
	if (iter == elementMapping.end()) {
		return nullptr;
	}
	return iter->second->createNode(parent);
}

NAMESPACE_DEBRIS_END
