#include "stdafx_core.h"
#include <scenenode.h>

NAMESPACE_DEBRIS_BEGIN

void SceneNode::endNode() {
	scene = boost::shared_ptr<Scene>(new Scene(primitives, lights, camera));
}

NAMESPACE_DEBRIS_END
