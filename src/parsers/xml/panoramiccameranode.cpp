#include "stdafx_core.h"
#include <panoramiccameranode.h>

#include <cameras/panoramiccamera.h>

NAMESPACE_DEBRIS_BEGIN

Camera *PanoramicCameraNode::instantiateCamera() const {
	return new PanoramicCamera(transform, shared_ptr<Film>(new Film(width, height)), fieldOfView, focalDistance);
}

NAMESPACE_DEBRIS_END
