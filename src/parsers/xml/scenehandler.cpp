#include "stdafx_core.h"
#include <scenehandler.h>

#include <boost/filesystem.hpp>

NAMESPACE_DEBRIS_BEGIN

SceneHandler::SceneHandler() {}

SceneHandler::~SceneHandler() {}

void SceneHandler::startElement(const XMLCh* const uri, const XMLCh* const localname,
	const XMLCh* const qname, const Attributes& attrs) {
	std::wstring elementName(localname);
	Node * node = elementMapping.createNode(elementName, currentNode);
	if (node == nullptr) {
		throw std::exception("Error");
	}
	else {
		if (rootNode == nullptr) {
			rootNode = node;
		}
		else {
			if (!currentNode->validateChild(elementName)) {
				throw SAXException("Invalid child...");
			}
		}
		node->startNode();
		node->bind(attrs);
		currentNode = node;
	}
}

void SceneHandler::endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	currentNode->endNode();
	if (currentNode->getType() != NODE_TYPE::SCENE) {
		Node *parentNode = currentNode->getParent();
		parentNode->addChild(currentNode);
		currentNode = parentNode;
	}
}

void SceneHandler::fatalError(const SAXParseException& exception) {
	char* message = XMLString::transcode(exception.getMessage());
	std::cout << "Fatal Error: " << message
		<< " at line: " << exception.getLineNumber()
		<< std::endl;
	XMLString::release(&message);
	throw exception;
}

void SceneHandler::error(const SAXParseException& exception) {
	char* message = XMLString::transcode(exception.getMessage());
	std::cout << "Error: " << message
		<< " at line: " << exception.getLineNumber()
		<< ", column: " << exception.getColumnNumber() << std::endl;
	XMLString::release(&message);
	throw exception;
}

SceneNode *SceneHandler::getSceneNode() const {
	return (SceneNode*)rootNode;
}

std::string getCurrentProcessDirectory() {
	char currentProcessDirectory[1024];
#ifdef _WIN32
	if (!GetModuleFileName(NULL, currentProcessDirectory, 1024)) {
		throw debris::DebrisException("An error occured while trying to load scene schema definition file");
	}
	return std::string(filesystem::path(currentProcessDirectory).remove_filename().string());
#endif
}

SAX2XMLReader *createConfiguredSAXParser() {
	SAX2XMLReader *xmlReader = XMLReaderFactory::createXMLReader();
	xmlReader->setFeature(XMLUni::fgSAX2CoreValidation, true);
	xmlReader->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);
	xmlReader->setFeature(XMLUni::fgXercesSchema, true);
	xmlReader->setFeature(XMLUni::fgXercesSchemaFullChecking, true);
	xmlReader->setFeature(XMLUni::fgXercesValidationErrorAsFatal, true);
	xmlReader->setFeature(XMLUni::fgXercesUseCachedGrammarInParse, true);
	xmlReader->setFeature(XMLUni::fgXercesLoadSchema, true);
	xmlReader->setValidationConstraintFatal(true);
	std::string schemaFullpath = getCurrentProcessDirectory();
	schemaFullpath += "\\data\\schema\\Debris.xsd";
	xmlReader->loadGrammar(schemaFullpath.c_str(), Grammar::SchemaGrammarType, true);
	return xmlReader;
}

NAMESPACE_DEBRIS_END
