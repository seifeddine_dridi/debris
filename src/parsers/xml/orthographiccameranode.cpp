#include "stdafx_core.h"
#include <orthographiccameranode.h>

#include <cameras/orthographiccamera.h>

NAMESPACE_DEBRIS_BEGIN

Camera *OrthographicCameraNode::instantiateCamera() const {
	return new OrthographicCamera(transform, shared_ptr<Film>(new Film(width, height)), zoom);
}

NAMESPACE_DEBRIS_END
