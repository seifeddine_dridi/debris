#include "stdafx_cwrapper.h"
#include <enginecwrapper.h>

struct _Handle {
	virtual ~_Handle() {}
	virtual void set(const std::string &property, const Variant& v) {}
	virtual void create() {}
};

template <class T>
struct BaseHandle : public _Handle {
	BaseHandle() {}
	BaseHandle(const shared_ptr<T> &instance) : instance(instance) {}
	shared_ptr<T> instance;
};

struct _FrameBufferHandle : public BaseHandle<Film> {};

template <typename T>
struct ConstHandle : public BaseHandle<T> {
	ConstHandle(const shared_ptr<T> &ptr) {
		instance = ptr;
	}
	void create() {
		std::cout << "Cannot modify constant handle" << std::endl;
	}
	void set(const std::string& property, const Variant& data) {
		std::cout << "Cannot modify constant handle" << std::endl;
	}
};

//struct _LightHandle : public BaseHandle<Light> {
//	void set(const std::string &property, const Variant& v) {
//		if (property == "position") {
//			pos = boost::get<Vector>(v);
//		} else if (property == "intensity") {
//			intensity = boost::get<float>(v);
//		} else if (property == "direction") {
//			dir = boost::get<Vector>(v);
//		} else {
//			std::cout << "Unsupported handle parameter " << property << std::endl;
//		}
//	}
//
//	Vector pos;
//	Vector dir;
//	float intensity;
//};
//
//struct PointLightHandle : public _LightHandle {
//	void create() {
//		instance = shared_ptr<Light>(new PointLight(pos, intensity));
//	}
//};
//
//struct DirectionalLightHandle : public _LightHandle {
//	void create() {
//		instance = shared_ptr<Light>(new DirectionalLight(dir.normalize(), intensity));
//	}
//};
//
//struct AreaLightHandle : public _LightHandle {
//	AreaLightHandle(ShapeHandle shape, float emittance): shape(shape), emittance(emittance) {}
//	void create() {
//	}
//	
//	ShapeHandle shape;
//	float emittance;
//};
//
//struct _CameraHandle : public BaseHandle<Camera> {};
//
//struct PerspectiveCameraHandle : public _CameraHandle {
//	void set(const std::string &property, const Variant& v) {
//		if (property == "position") {
//			pos = boost::get<Vector>(v);
//		} else if (property == "transform") {
//			affineSpace = boost::get<AffineSpace>(v);
//		} else if (property == "lens-radius") {
//			lensRadius = boost::get<float>(v);
//		} else if (property == "focal-distance") {
//			focalDistance = boost::get<float>(v);
//		} else {
//			std::cout << "Unsupported handle parameter " << property << std::endl;
//		}
//	}
//	void create() {
//		instance = shared_ptr<Camera>(new PerspectiveCamera(pos, affineSpace, lensRadius, focalDistance));
//	}
//
//	AffineSpace affineSpace;
//	Vector pos;
//	float lensRadius;
//	float focalDistance;
//};
//
//struct _TextureHandle : public BaseHandle<Texture> {
//	void set(const std::string &property, const Variant &variant) {
//		std::cout << "Unsupported handle parameter " << property << std::endl;
//	}
//};
//
//struct UniformTextureHandle : public _TextureHandle {
//	UniformTextureHandle() {
//		value = 0;
//	}
//	void set(const std::string &property, const Variant &variant) {
//		if (property == "scale") {
//			value = boost::get<float>(variant);
//		} else if (property == "wavelength") {
//			wavelength = boost::get<float>(variant);
//		} else if (property == "rgbcolor") {
//			rgbColor = boost::get<Vector>(variant);
//		} else {
//			_TextureHandle::set(property, variant);
//		}
//	}
//	void create() {
//		if (value != 0) {
//			instance = shared_ptr<Texture>(new UniformTexture(wavelength, value));
//		} else {
//			instance = shared_ptr<Texture>(new UniformTexture(rgbColor));
//		}
//	}
//	
//	float value;
//	float wavelength;
//	Vector rgbColor;
//};
//
//struct ImageHandle : public _TextureHandle {
//	void set(const std::string &property, const Variant &variant) {
//		if (property == "imagesrc") {
//			imageSrc = boost::get<std::string>(variant);
//		} else {
//			_TextureHandle::set(property, variant);
//		}
//	}
//	void create() {
//		instance = shared_ptr<Texture>(new ImageTexture(imageSrc));
//	}
//	
//	std::string imageSrc;
//};
//
//struct CheckerBoardTextureHandle : public _TextureHandle {
//	void set(const std::string &property, const Variant& v) {
//		if (property == "sizex") {
//			sizex = boost::get<float>(v);
//		} else if (property == "sizey") {
//			sizey = boost::get<float>(v);
//		} else {
//			_TextureHandle::set(property, v);
//		}
//	}
//	void create() {
//		instance = shared_ptr<Texture>(new CheckerBoard(sizex, sizey));
//	}
//	
//	float sizex, sizey;
//};
//
//struct _ShaderHandle : public BaseHandle<Shader> {
//	_ShaderHandle(TextureHandle texture = NULL): texture(texture) {
//		emittance = 0;
//	}
//	~_ShaderHandle() {
//		if (texture) {
//			delete texture;
//		}
//	}
//	void set(const std::string &property, const Variant& v) {
//		if (property == "spectrum") {
//			wavelength = boost::get<float>(v);
//		} else if (property == "emittance") {
//			emittance = boost::get<float>(v);
//		} else if (property == "texture") {
//
//		} else {
//			std::cout << "Unsupported handle parameter " << property << std::endl;
//		}
//	}
//	void create() {
//		if (texture != NULL) {
//			texture->create();
//		}
//	}
//
//	Vector spectrum;
//	float wavelength;
//	float emittance;
//	TextureHandle texture;
//};
//
//struct DiffuseShaderHandle : public _ShaderHandle {
//	DiffuseShaderHandle(TextureHandle texture = NULL): _ShaderHandle(texture) {
//		kd = 0.8f;
//	}
//	void set(const std::string &property, const Variant& v) {
//		if (property == "kd") {
//			kd = boost::get<float>(v);
//		} else {
//			_ShaderHandle::set(property, v);
//		}
//	}
//	void create() {
//		_ShaderHandle::create();
//		instance = shared_ptr<Shader>(new DiffuseShader(texture->instance, kd, emittance));
//	}
//
//	float kd;
//};
//
//struct SpecularShaderHandle : public _ShaderHandle {
//	SpecularShaderHandle(TextureHandle texture = NULL): _ShaderHandle(texture) {
//		kr = 0.8f;
//	}
//	void set(const std::string &property, const Variant& v) {
//		if (property == "kr") {
//			kr = boost::get<float>(v);
//		} else {
//			_ShaderHandle::set(property, v);
//		}
//	}
//	void create() {
//		_ShaderHandle::create();
//		instance = shared_ptr<Shader>(new MirrorShader(texture->instance, kr, emittance));
//	}
//	
//	float kr;
//};
//
//struct _ShapeHandle : public BaseHandle<AbstractShape> {
//	void set(const std::string &property, const Variant &variant) {
//		std::cout << "Unsupported handle parameter " << property << std::endl;
//	}
//};
//
//struct SphereHandle : public _ShapeHandle {
//	void set(const std::string &property, const Variant& v) {
//		if (property == "radius") {
//			radius = boost::get<float>(v);
//		} else if (property == "position") {
//			pos = boost::get<Vector>(v);
//		} else if (property == "transform") {
//			affineSpace = boost::get<AffineSpace>(v);
//		} else {
//			_ShapeHandle::set(property, v);
//		}
//	}
//	void create() {
//		instance = shared_ptr<AbstractShape>(new Sphere(radius, pos, affineSpace));
//	}
//
//	AffineSpace affineSpace;
//	float radius;
//	Vector pos;
//};
//
//struct _PrimitiveHandle : public BaseHandle<Primitive> {
//	_PrimitiveHandle(const ShapeHandle shape, const ShaderHandle shader): shape(shape), shader(shader), light(NULL) {}
//	void set(const std::string &property, const Variant& v) {
//		std::cout << "Cannot modify constant handle" << std::endl;
//	}
//	_PrimitiveHandle(const LightHandle light): light(light) {}
//	void create() {
//		if (light != NULL) {
//			light->create();
//		} else {
//			shape->create();
//			shader->create();
//			instance = shared_ptr<Primitive>(new Primitive(shape->instance, shader->instance));
//		}
//	}
//	~_PrimitiveHandle() {
//		if (light) {
//			delete light;				
//		} else {
//			delete shape;
//			delete shader;
//		}
//	}
//
//	ShapeHandle shape;
//	ShaderHandle shader;
//	LightHandle light;
//};
//
//struct _SceneHandle : public BaseHandle<Scene> {
//	_SceneHandle(const shared_ptr<debris::Scene> &instance): BaseHandle(instance) {}
//	_SceneHandle(PrimitiveHandle *prims, int size) {
//		// Extract all shapes and lights
//		for (int i = 0; i < size; ++i) {
//			PrimitiveHandle primitive = prims[i];
//			if (primitive->light) {
//				lightsList.push_back(primitive->light);
//			} else {
//				shapesList.push_back(primitive);
//			}
//		}
//	}
//	~_SceneHandle() {
//		for (uint32_t i = 0; i < shapesList.size(); ++i) {
//			delete shapesList[i];
//		}
//		for (uint32_t i = 0; i < lightsList.size(); ++i) {
//			delete lightsList[i];
//		}
//	}
//
//	std::vector<PrimitiveHandle> shapesList;
//	std::vector<LightHandle> lightsList;
//};
//
//struct NoAccelSceneHandle : public _SceneHandle {
//	NoAccelSceneHandle(PrimitiveHandle *prims, int size): _SceneHandle(prims, size) {}
//	void create() {
//		// Extract and create lights
//		std::vector<shared_ptr<Light>> lights;
//		for (size_t i = 0; i < lightsList.size(); ++i) {
//			lightsList[i]->create();
//			lights.push_back(lightsList[i]->instance);
//		}
//		std::vector<shared_ptr<Primitive>> primitives;
//		primitives.reserve(shapesList.size());
//		// Extract and create shapes
//		for (uint32_t i = 0; i < shapesList.size(); ++i) {
//			shapesList[i]->create();
//			primitives.push_back(shapesList[i]->instance);
//			if (shapesList[i]->instance->isEmitter()) {
//				// Shape is a light emitter
//				lights.push_back(shared_ptr<Light>(new AreaLight(shapesList[i]->instance)));
//			}
//		}
//		//instance = shared_ptr<Scene>(new Scene(primitives, lights));
//		//instance->build(shared_ptr<Accelerator>(new NaiveAccelerator()));
//	}
//};
//
//struct ConstSceneHandle : public _SceneHandle {
//	ConstSceneHandle(const shared_ptr<debris::Scene> &instance) : _SceneHandle(instance) {}
//	void create() {
//		std::cout << "Cannot modify constant handle" << std::endl;
//	}
//	void set(const std::string& property, const Variant& data) {
//		std::cout << "Cannot modify constant handle" << std::endl;
//	}
//};
//
//struct _RendererHandle : public BaseHandle<Renderer> {};
//
//struct DefaultRendererHandle : public _RendererHandle {
//	void create() {
//		instance = shared_ptr<Renderer>(new DefaultRenderer(shared_ptr<LightIntegrator>(new WhittedIntegrator(10))));
//	}
//};
//
//struct _RenderingConfig : public _Handle {
//	_RenderingConfig(const RendererHandle renderer, const CameraHandle camera, const SceneHandle scene) {
//		this->renderer = renderer;
//		this->camera = camera;
//		this->scene = scene;
//	}
//	~_RenderingConfig() {
//		delete renderer;
//		delete camera;
//		delete scene;
//	}
//
//	RendererHandle renderer;
//	CameraHandle camera;
//	SceneHandle scene;
//};
//
//RenderingConfig parseSceneFile(const char *filename) {
//	try {
//		//SceneParser parser;
//		return nullptr;
//		//return parser.parse(filename);
//	} catch (const std::exception &e) {
//		std::cout << e.what() << std::endl;
//	}
//}
//
//LightHandle createLight(const char *type) {
//	std::string light(type);
//	if (light == "point") {
//		return new PointLightHandle();
//	} else if (light == "directional") {
//		return new DirectionalLightHandle();
//	} else {
//		std::cout << "Unrecognized light type (" << light << ") in createLight()" << std::endl;
//		return NULL;
//	}
//}
//
//TextureHandle createTexture(const char *type) {
//	std::string texture(type);
//	if (texture == "image") {
//		return new ImageHandle();
//	} else if (texture == "checker") {
//		return new CheckerBoardTextureHandle();
//	} else if (texture == "uniform") {
//		return new UniformTextureHandle();
//	} else {
//		std::cout << "Unsupported texture type " + texture << std::endl;
//		return NULL;
//	}
//}
//
//ShaderHandle createShader(const char *type) {
//	std::string shader(type);
//	if (shader == "diffuse") {
//		return new DiffuseShaderHandle();
//	} else if (shader == "mirror") {
//		return new SpecularShaderHandle();
//	} else {
//		std::cout << "Unrecognized shader type (" << shader << ")" << std::endl;
//		return NULL;
//	}
//}
//
//ShaderHandle createTexturedShader(const char *type, TextureHandle texture) {
//	std::string shader(type);
//	if (shader == "diffuse") {
//		return new DiffuseShaderHandle(texture);
//	} else if (shader == "mirror") {
//		return new SpecularShaderHandle(texture);
//	} else {
//		std::cout << "Unrecognized shader type (" << shader << ")" << std::endl;
//		return NULL;
//	}
//}
//
//ShapeHandle createShape(const char *type) {
//	std::string shape(type);
//	if (shape == "sphere") {
//		return new SphereHandle();
//	} else {
//		std::cout << "Unrecognized shape type (" << shape << ")" << std::endl;
//		return NULL;
//	}
//}
//
//PrimitiveHandle createPrimitiveShape(ShapeHandle shape, ShaderHandle shader) {
//	return new _PrimitiveHandle(shape, shader);
//}
//
//PrimitiveHandle createPrimitiveLight(LightHandle light) {
//	return new _PrimitiveHandle(light);
//}
//
//CameraHandle createCamera(const char *type) {
//	std::string camera(type);
//	if (camera == "perspective") {
//		return new PerspectiveCameraHandle();
//	} else {
//		std::cout << "Unrecognized camera type (" << camera << ")" << std::endl;
//		return NULL;
//	}
//}
//
//SceneHandle createScene(const char *type, PrimitiveHandle *prims, int size) {
//	std::string scene(type);
//	if (scene == "naive") {
//		return new NoAccelSceneHandle(prims, size);
//	} else {
//		std::cout << "Unrecognized scene type (" << scene << ")" << std::endl;
//		return NULL;
//	}
//}
//
//RendererHandle createRenderer(const char *type) {
//	std::string renderer(type);
//	if (renderer == "default") {
//		return new DefaultRendererHandle();
//	} else {
//		std::cout << "Unrecognized renderer type (" << renderer << ")" << std::endl;
//		return NULL;
//	}
//}
//
//RenderingConfig createRenderingConfig(const RendererHandle renderer, const CameraHandle camera, const SceneHandle scene) {
//	return new _RenderingConfig(renderer, camera, scene);
//}
//
//FrameBufferHandle createFrameBuffer(const char *type, int width, int height) {
//	return (FrameBufferHandle) new ConstHandle<Film>(shared_ptr<Film>(new Film(width, height)));
//}
//
//void saveFramebuffer(FrameBufferHandle frameBufferHandle, const char *filepath) {
//	frameBufferHandle->instance->writeToFile(filepath);
//}
//
//void deleteHandle(Handle handle) {
//	if (handle != NULL) {
//		delete handle;
//	}
//}
//
//void setFloat1Attribute(Handle handle, const char *property, float x) {
//	handle->set(property, Variant(x));
//}
//
//void setFloat3Attribute(Handle handle, const char *property, float x, float y, float z) {
//	handle->set(property, Variant(Vector(x, y, z)));
//}
//
//void setMatrix33Attribute(Handle handle, const char *property, float *mat) {
//	handle->set(property, Variant(AffineSpace(Vector(mat[0], mat[1], mat[2]), Vector(mat[3], mat[4], mat[5]), Vector(mat[6], mat[7], mat[8]))));
//}
//
//void setStringAttribute(Handle handle, const char *property, const char *value) {
//	handle->set(property, Variant(std::string(value)));
//}

int renderFrameFromSceneFile(const char *filename, const char *outputImage, int spp, unsigned nbThreads) {
	try {
		XMLPlatformUtils::Initialize();
		SAX2XMLReader *xmlReader = createConfiguredSAXParser();

		SceneHandler sceneHandler;
		xmlReader->setContentHandler(&sceneHandler);
		xmlReader->setErrorHandler(&sceneHandler);
		try {
			xmlReader->parse(filename);
		}
		catch (const SAXException &ex) {
			char* message = XMLString::transcode(ex.getMessage());
			std::cout << "Invalid scene file: " << message << std::endl;
			XMLString::release(&message);
			return -1;
		}
		std::cout << "Input scene file is valid" << std::endl;
		XMLPlatformUtils::Terminate();

		SceneNode *sceneNode = sceneHandler.getSceneNode();
		shared_ptr<Renderer> renderer(new DefaultRenderer(shared_ptr<LightIntegrator>(new WhittedIntegrator(10))));
		boost::timer::auto_cpu_timer cpuTimer;
		RenderTaskManager taskManager(nbThreads, renderer, sceneNode->scene, spp,
			sceneNode->camera->getFilm()->imageWidth, sceneNode->camera->getFilm()->imageHeight);
		taskManager.execute();
		while (taskManager.getExecutionState() != TASKS_EXECUTION_STATE::FINISHED) {
			//std::cout << "Rendering progress: " << taskManager.getProgress() << "\r";
		}
		std::cout << std::endl;
		sceneNode->camera->getFilm()->writeToFile(outputImage);
		return 0;
	} catch (const DebrisException& ex) {
		std::cout << ex.what() << std::endl;
		return -1;
	}
}

//SceneHandle loadScene(const char *filename) {
//	try {
//		XMLPlatformUtils::Initialize();
//		SAX2XMLReader *xmlReader = createConfiguredSAXParser();
//
//		SceneHandler sceneHandler;
//		xmlReader->setContentHandler(&sceneHandler);
//		xmlReader->setErrorHandler(&sceneHandler);
//		try {
//			xmlReader->parse(filename);
//		}
//		catch (const SAXException &ex) {
//			char* message = XMLString::transcode(ex.getMessage());
//			std::cout << "Invalid XML file: " << message << std::endl;
//			XMLString::release(&message);
//			return NULL;
//		}
//		std::cout << "XML file is valid" << std::endl;
//		XMLPlatformUtils::Terminate();
//
//		SceneNode *sceneNode = sceneHandler.getSceneNode();
//		return new ConstSceneHandle(sceneNode->scene);
//	}
//	catch (const DebrisException& ex) {
//		std::cout << ex.what() << std::endl;
//		return NULL;
//	}
//}

//void renderFrame(RendererHandle renderer, CameraHandle camera, SceneHandle scene, FrameBufferHandle frameBuffer, int spp) {
//	renderer->create();
//	camera->create();
//	scene->create();
//	boost::timer::auto_cpu_timer t;
//	renderer->instance->renderFrame(scene->instance, 0, spp, ImageSlice(0, frameBuffer->instance->imageWidth, 0, frameBuffer->instance->imageHeight));
//}

//void renderFrame2(SceneHandle scene, const char *outputImage, int spp) {
//	shared_ptr<Renderer> renderer(new DefaultRenderer(shared_ptr<LightIntegrator>(new WhittedIntegrator(10))));
//	boost::timer::auto_cpu_timer t;
//	RenderScheduler scheduler(4, renderer, scene->instance->getCamera()->getFilm()->imageWidth, scene->instance->getCamera()->getFilm()->imageHeight);
//	scheduler.renderFrame(scene->instance, 0, spp);
//	scene->instance->getCamera()->getFilm()->writeToFile(outputImage);
//}

unsigned getMaximumThreadCount() {
	return boost::thread::hardware_concurrency();
}
