#pragma once

#include <cstdint>

#include <core/debris.h>

NAMESPACE_DEBRIS_BEGIN

/* Period parameters for the Mersenne Twister RNG */
#define MT_N 624
#define MT_M 397
#define MT_MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define MT_UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define MT_LOWER_MASK 0x7fffffffUL /* least significant r bits */

/**
 * \brief Mersenne Twister: pseudorandom number generator based on a
 * twisted generalized feedback shift register
 *
 * Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
 * All rights reserved.                          
 */
class DEBRISCORE_API Random {
public:
	/// Create an uninitialized instance
	Random();

	/// Seed the RNG with the specified seed value
	void seed(uint32_t value);
	
	/// Seed the RNG with an entire array 
	void seed(uint32_t *values, int length);
	
	/// Seed the RNG using an existing instance
	void seed(Random *random);

	/// Generate an uniformly distributed 32-bit integer
	uint32_t nextUInt();

	/// Generate an uniformly distributed single precision value on [0,1)
	float nextFloat();
private:
	uint32_t m_mt[MT_N];
	int m_mti;
};

NAMESPACE_DEBRIS_END
