#pragma once

#include <core/spectrum.h>
#include <core/geometry/affinespace.h>

#include <map>
#include <boost/variant.hpp>

NAMESPACE_DEBRIS_BEGIN

typedef boost::variant<int, float, bool, Vector, AffineSpace, std::string, XYZColor> Variant;

class DEBRISCORE_API PropertyList {
public:
	void addInt(const std::string &property, const int &value);
	int getInt(const std::string &property) const;
	int getInt(const std::string &property, const int &defaultValue) const;
	void addFloat(const std::string &property, const float &value);
	float getFloat(const std::string &property) const;
	float getFloat(const std::string &property, const float &defaultValue) const;
	void addString(const std::string &property, const std::string &value);
	std::string getString(const std::string &property) const;
	std::string getString(const std::string &property, const std::string &defaultValue) const;
	void addAffineSpace(const std::string &property, const AffineSpace &value);
	AffineSpace getAffineSpace(const std::string &property) const;
	AffineSpace getAffineSpace(const std::string &property, const AffineSpace &defaultValue) const;

private:
	std::map<std::string, Variant> props;
};
	
NAMESPACE_DEBRIS_END
