#pragma once

#include <iostream>
#include <boost/shared_ptr.hpp>

using namespace boost;

#define NAMESPACE_DEBRIS_BEGIN namespace debris {
#define NAMESPACE_DEBRIS_END }

#include <core/system/platform.h>
#include <core/system/debrisexception.h>
#include <core/constants.h>
#include <core/classmanifest.h>

#include <Eigen/Geometry>
#include <Eigen/src/Core/util/Constants.h>
