#pragma once

#include <core/spectrum.h>
#include <core/geometry/ray.h>

NAMESPACE_DEBRIS_BEGIN

class LightIntegrator {
public:
	virtual ~LightIntegrator() {}
	virtual XYZColor getRadiance(const Ray &ray, const shared_ptr<Scene> &scene, float wavelength, Random &rng) const = 0;
};

NAMESPACE_DEBRIS_END
