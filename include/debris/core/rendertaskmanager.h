#pragma once

#include <core/system/taskmanager.h>
#include <core/renderer.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API RenderTaskManager : public TaskManager {
public:
	RenderTaskManager(unsigned nbThreads, shared_ptr<Renderer> &renderer, const shared_ptr<Scene> &scene, int spp, int imageWidth, int imageHeight);
	~RenderTaskManager();

private:
	shared_ptr<ImageSlicer> tiles;
};

NAMESPACE_DEBRIS_END
