#pragma once

#include <core/geometry/abstractshape.h>
#include <core/shading/shader.h>

NAMESPACE_DEBRIS_BEGIN

struct IntersectionRecord {
	DifferentialGeometry diffGeometry;
	shared_ptr<Shader> shader;
};

class DEBRISCORE_API Primitive {
public:
	Primitive(const shared_ptr<AbstractShape> &shape, const shared_ptr<Shader> &shader): shape(shape), shader(shader) {}
	void postIntersection(const RTCRay &ray, IntersectionRecord *record) const;
	const shared_ptr<AbstractShape> &getShape() const;
	const shared_ptr<Shader> &getShader() const;
	bool isEmitter() const;

private:
	const shared_ptr<AbstractShape> shape;
	const shared_ptr<Shader> shader;
};

NAMESPACE_DEBRIS_END
