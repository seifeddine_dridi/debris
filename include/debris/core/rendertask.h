#pragma once

#include <core/system/task.h>
#include <core/imageslicer.h>

NAMESPACE_DEBRIS_BEGIN

class Renderer;

class DEBRISCORE_API RenderTask : public Task {
public:
	RenderTask(Renderer *renderer, const shared_ptr<Scene> &scene, float time, int spp,
		ImageSlice tile, TaskEventBroadcaster *renderStatistics);
	void run();

private:
	Renderer *renderer;
	shared_ptr<Scene> scene;
	float time;
	int spp;
	ImageSlice tile;
};

NAMESPACE_DEBRIS_END
