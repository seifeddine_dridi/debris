#pragma once

#include <core/spectrum.h>

#include <fstream>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API Film {
public:
	Film(int width, int height);
	~Film();
	void set(int x, int y, const XYZColor &c);
	void add(int x, int y, const XYZColor &c);
	void multiply(float scalar);
	/*! Write PPM file */
	bool writeToFile(const char *filename) const;

	const int imageWidth, imageHeight;
	const float invWidth, invHeight;

protected:
	XYZColor *frameBuffer;
};

NAMESPACE_DEBRIS_END
