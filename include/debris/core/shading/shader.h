#pragma once

#include <core/texture.h>
#include <core/shading/bsdf.h>
#include <core/geometry/differentialgeometry.h>

NAMESPACE_DEBRIS_BEGIN

using namespace boost;

#define NEW_BSDF(BSDF, ...) new(bsdfContainer.getMemoryChunk()) BSDF##(__VA_ARGS__)

class BSDFContainer {
public:
	BSDF *getBSDF() const {
		return bsdf;
	}
	char *getMemoryChunk() {
		return bytes;
	}
	void setBSDF(BSDF *bsdf) {
		this->bsdf = bsdf;
	}

private:
#define MAX_BYTES 128
	char bytes[MAX_BYTES];
	BSDF *bsdf;
};

class DEBRISCORE_API Shader {
public:
	Shader(const shared_ptr<Texture> &texture, float emittance): texture(texture), emittance(emittance) {}
	virtual void createBSDF(const DifferentialGeometry &dGeom, float wavelength, BSDFContainer &bsdfContainer) const = 0;
	XYZColor getColor(float u, float v) const;
	bool isEmitter() const;
	float getEmittance() const;
	XYZColor getEmittedSpectrum(const DifferentialGeometry &dGeom) const;

protected:
	shared_ptr<Texture> texture;
	float emittance;
};

NAMESPACE_DEBRIS_END
