#pragma once

#include <core/random.h>
#include <core/spectrum.h>
#include <core/geometry/affinespace.h>

NAMESPACE_DEBRIS_BEGIN

enum ReflectionType {
	DIFFUSE, SPECULAR
};

class BSDF {
public:
	BSDF(const XYZColor &spectrum) : spectrum(spectrum) {}
	/**
	 * @param wi incoming direction in world space
	 * @param wo outgoing direction in world space
	 * @return pdf of the sampled direction (wi)
	 */
	virtual XYZColor sample(Random &rng, Vector &wi, const Vector &wo, const Vector &normal) const = 0;
	virtual XYZColor eval(const Vector &wi, const Vector &wo, const Vector &normal) const = 0;
	virtual float getPdf(const Vector &wi, const Vector &wo, const Vector &normal) const = 0;
	virtual ReflectionType getType() const = 0;
	
protected:
	XYZColor spectrum;
};

NAMESPACE_DEBRIS_END
