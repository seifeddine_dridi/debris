#pragma once

#include <core/geometry/vector.h>

NAMESPACE_DEBRIS_BEGIN

inline float degreeToRadians(float deg) {
	return deg / 180.0f * (float)M_PI;
}

inline float centimeterToMeter(float cm) {
	return cm * 0.01f;
}

inline float nanometerToCentimeter(float nm) {
	return nm * 1e-7f;
}

inline float nanometerToMeter(float nm) {
	return nm * 1e-9f;
}

inline Vector getVector(float phi, float theta) {
	return Vector(cosf(phi) * sinf(theta), cosf(theta), sinf(phi) * sinf(theta));
}

NAMESPACE_DEBRIS_END
