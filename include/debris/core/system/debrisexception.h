#pragma once

#include <exception>
#include <string>

NAMESPACE_DEBRIS_BEGIN

class DebrisException : public std::exception {
public:
	DebrisException(const std::string &message): std::exception(message.c_str()) {}
};

NAMESPACE_DEBRIS_END
