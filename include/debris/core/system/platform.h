#ifdef DEBRISCORE_EXPORTS
#define DEBRISCORE_API __declspec(dllexport)
#else
#define DEBRISCORE_API __declspec(dllimport)
#endif

#if defined(WIN32) && !defined(__CYGWIN__)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#endif
