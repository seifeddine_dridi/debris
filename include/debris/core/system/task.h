#pragma once

#include <taskeventbroadcaster.h>

NAMESPACE_DEBRIS_BEGIN

class Task {
public:
	Task(unsigned id, TaskEventBroadcaster *tasksStatistics) : id(id), tasksStatistics(tasksStatistics) {}
	virtual void run() = 0;

protected:
	unsigned id;
	TaskEventBroadcaster *tasksStatistics;
};

NAMESPACE_DEBRIS_END
