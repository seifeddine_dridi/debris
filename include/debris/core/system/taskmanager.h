#pragma once

#include <boost/thread.hpp>
#include <boost/asio.hpp>

#include <taskeventbroadcaster.h>

NAMESPACE_DEBRIS_BEGIN

class Task;

enum TASKS_EXECUTION_STATE { IDLE, RUNNING, FINISHED, SUSPENDED };

class DEBRISCORE_API TaskManager {
public:
	TaskManager(unsigned nbThreads);
	~TaskManager();
	void postTask(Task *task);
	void execute();
	void pause();
	void resume();
	void finish();
	float getProgress() const;
	TaskEventBroadcaster *getTaskEventBroadcaster();
	TASKS_EXECUTION_STATE getExecutionState() const;
	unsigned getNbTasks() const;

protected:
	TASKS_EXECUTION_STATE executionState;
	boost::asio::io_service service;
	boost::thread_group threadpool;
	boost::asio::io_service::work work;
	unsigned nbTasks;
	TaskEventBroadcaster tasksStatistics;
};

NAMESPACE_DEBRIS_END
