#pragma once

#include <core/debris.h>
#include <boost/thread/mutex.hpp>

NAMESPACE_DEBRIS_BEGIN

class TaskManager;

enum TASK_STATISTICS { NB_FINISHED_TASKS, NB_RUNNING_TASKS, NB_STARTED_TASKS };

class DEBRISCORE_API TaskEventBroadcaster {
public:
	TaskEventBroadcaster(TaskManager *taskManager);
	void taskStarted(unsigned taskId);
	void taskEnded(unsigned taskId);
	unsigned getStatistics(TASK_STATISTICS statType) const;
	void notifyTasks();

private:
	unsigned stats[3];
	TaskManager *taskManager;
	boost::mutex mutex;
};

NAMESPACE_DEBRIS_END
