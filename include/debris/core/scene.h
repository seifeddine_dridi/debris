#pragma once

#include <core/primitive.h>
#include <core/light.h>
#include <core/camera.h>

NAMESPACE_DEBRIS_BEGIN

typedef std::vector<shared_ptr<Light>> LightList;
typedef std::vector<shared_ptr<Primitive>> PrimitiveList;

class DEBRISCORE_API Scene {
public:
	Scene(const PrimitiveList &primitives, const LightList &lights, const shared_ptr<Camera> &camera);
	~Scene();
	bool intersect(const Ray &ray, IntersectionRecord *info) const;
	bool intersectP(const Ray &ray) const;
	const LightList &getLights() const {
		return lights;
	}
	void addPrimitive(const shared_ptr<Primitive> &primitive);
	void addLight(const shared_ptr<Light> &light);
	void setCamera(const shared_ptr<Camera> &camera);
	shared_ptr<Camera> &getCamera();
	const shared_ptr<Light> &getBackgroundLight() const;
	XYZColor getBackgroundRadiance(const Ray &ray) const;
	RTCScene &getEmbreeSceneObject() {
		return scene;
	}
	void updateScene(float time) const;

private:
	RTCScene scene;
	PrimitiveList primitives;
	LightList lights;
	shared_ptr<Light> envLight;
	shared_ptr<Camera> camera;
};

NAMESPACE_DEBRIS_END
