#pragma once

#include <core/scene.h>
#include <core/imageslicer.h>

NAMESPACE_DEBRIS_BEGIN

class Renderer {
public:
	Renderer() {}
	virtual ~Renderer() {}
	virtual void renderFrame(const shared_ptr<Scene> &scene, float time, int spp, const ImageSlice &tile) = 0;
};

NAMESPACE_DEBRIS_END
