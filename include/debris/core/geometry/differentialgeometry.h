#pragma once

#include <core/geometry/vector.h>

NAMESPACE_DEBRIS_BEGIN

struct DifferentialGeometry {
	Vector pos;
	Vector normal;
	float uv[2];
	float distance;
};

NAMESPACE_DEBRIS_END
