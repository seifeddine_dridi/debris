#pragma once

#include <transform.h>

NAMESPACE_DEBRIS_BEGIN

class AffineSpace {
public:
	AffineSpace() {
		this->uAxis = Vector(1.0f);
		this->vAxis = Vector(0, 1.0f);
		this->wAxis = Vector(0, 0, 1.0f);
	}
	AffineSpace(const Vector &u, const Vector &v, const Vector &w) {
		this->uAxis = u;
		this->vAxis = v;
		this->wAxis = w;
	}
	AffineSpace(const Vector &wAxis) {
		this->wAxis = wAxis;
		makeCoordinateSystem(uAxis, vAxis, wAxis);
	}
	Vector operator() (const Vector &v) const {
		return uAxis * v.x + vAxis * v.y + wAxis * v.z;
	}

protected:
	void makeCoordinateSystem(Vector &u, Vector &v, const Vector &w) {
		Vector coVec;
		if (fabs(w.x) <= fabs(w.y)) {
			if (fabs(w.x) <= fabs(w.z)) { // yz plane
				coVec = Vector(0, -w.z, w.y);
			} else {
				coVec = Vector(-w.y, w.x, 0); // xy plane
			}
		} else if (fabs(w.y) <= fabs(w.z)) {
			coVec = Vector(-w.z, 0, w.x); // xz plane
		} else {
			coVec = Vector(-w.y, w.x, 0); // xy plane
		}
		coVec.normalize();
		u = w % coVec;
		v = u % w;
	}

public:
	Vector uAxis, vAxis, wAxis;
};

NAMESPACE_DEBRIS_END
