#pragma once

#include <core/geometry/ray.h>

NAMESPACE_DEBRIS_BEGIN

class BBox {
public:
	BBox() {
		pmin = Vector(INFINITY, INFINITY, INFINITY);
		pmax = Vector(-INFINITY, -INFINITY, -INFINITY);
	}
	BBox(const Vector &p1, const Vector &p2) {
		pmin = Vector(std::min(p1.x, p2.x), std::min(p1.y, p2.y), std::min(p1.z, p2.z));
		pmax = Vector(std::max(p1.x, p2.x), std::max(p1.y, p2.y), std::max(p1.z, p2.z));
	}
	bool intersect(const Ray &ray, float *tnear = nullptr, float *tfar = nullptr) const;

	Vector pmin, pmax;
};

NAMESPACE_DEBRIS_END
