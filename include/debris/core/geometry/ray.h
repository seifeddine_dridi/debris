#pragma once

#include <core/geometry/vector.h>

NAMESPACE_DEBRIS_BEGIN

struct Ray {
	Ray(const Vector &o, const Vector &d, float maxt = INFINITY, float time = 0.0f) {
		this->o = o;
		this->d = d;
		this->d.normalize();
		this->mint = EPSILON;
		this->maxt = maxt;
		this->time = time;
	}
	Vector operator() (float t) const {
		return o + d * t;
	}

	Vector o, d;
	float mint, maxt; // The ray interval span
	float time;
};

NAMESPACE_DEBRIS_END
