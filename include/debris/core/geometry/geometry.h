#pragma once

#include <core/geometry/differentialgeometry.h>
#include <core/geometry/affinespace.h>
#include <core/geometry/bbox.h>
#include <core/mathutils.h>