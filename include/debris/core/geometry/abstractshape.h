#pragma once

#include <core/geometry/geometry.h>

#include <embree2/rtcore.h>
#include <embree2/rtcore_ray.h>

NAMESPACE_DEBRIS_BEGIN

class AbstractShape {
public:
	/**
	* The following two functions are only included to facilitate unit testing.
	* I should probably change their names to avoid any conflict that might arise
	* due to C++ name hiding issues */
	bool intersect(const Ray &r, DifferentialGeometry &dGeom) const {
		RTCRay ray = toRTCRay(r);
		if (intersect(ray)) {
			dGeom.distance = ray.tfar;
			dGeom.normal = Vector(ray.Ng[0], ray.Ng[1], ray.Ng[2]);
			dGeom.uv[0] = ray.u;
			dGeom.uv[1] = ray.v;
			dGeom.pos[0] = ray.org[0] + ray.dir[0] * ray.tfar;
			dGeom.pos[1] = ray.org[1] + ray.dir[1] * ray.tfar;
			dGeom.pos[2] = ray.org[2] + ray.dir[2] * ray.tfar;
			return true;
		}
		return false;
	}
	bool intersectP(const Ray &r) const {
		RTCRay ray = toRTCRay(r);
		return intersectP(ray);
	}
	virtual bool intersect(RTCRay& ray) const = 0;
	virtual bool intersectP(RTCRay& ray) const = 0;
	virtual void postIntersect(const RTCRay &ray, DifferentialGeometry &dg) const {
		dg.distance = ray.tfar;
		dg.normal = Vector(ray.Ng[0], ray.Ng[1], ray.Ng[2]).normalize();
		dg.pos = Vector(ray.org[0] + ray.dir[0] * ray.tfar,
			ray.org[1] + ray.dir[1] * ray.tfar,
			ray.org[2] + ray.dir[2] * ray.tfar);
		dg.uv[0] = ray.u;
		dg.uv[1] = ray.v;
	}
	virtual bool isInside(const Vector &p) const = 0;
	virtual Vector sampleArea(float u1, float u2, float &pdf) const = 0;
	virtual Vector getNormal(const Vector &point) const = 0;
	virtual void setAnimation(const AnimatedTransform &animatedTransform) {
		interpolatedTransform = InterpolatedTransform(local2world, animatedTransform);
	}
	virtual void registerShape(RTCScene &scene) const = 0;
	virtual void updateShape(const RTCScene &scene, float time) const {}

protected:
	static Ray toRay(const RTCRay &ray) {
		Vector org(ray.org[0], ray.org[1], ray.org[2]);
		Vector dir(ray.dir[0], ray.dir[1], ray.dir[2]);
		return Ray(org, dir, ray.tfar, ray.time);
	}
	static RTCRay toRTCRay(const Ray &r) {
		RTCRay ray;
		ray.org[0] = r.o.x;
		ray.org[1] = r.o.y;
		ray.org[2] = r.o.z;
		ray.dir[0] = r.d.x;
		ray.dir[1] = r.d.y;
		ray.dir[2] = r.d.z;
		ray.tfar = r.maxt;
		ray.tnear = r.mint;
		ray.time = ray.time;
		ray.geomID = RTC_INVALID_GEOMETRY_ID;
		ray.primID = RTC_INVALID_GEOMETRY_ID;
		return ray;
	}

	Transform local2world;
	InterpolatedTransform interpolatedTransform;
	mutable unsigned int geomID;
};

NAMESPACE_DEBRIS_END
