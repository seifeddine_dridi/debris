#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include <core/debris.h>

NAMESPACE_DEBRIS_BEGIN

/**
 * Simple 3D Vector class
 */
class Vector {
public:
	Vector(float x = 0.0f, float y = 0.0f, float z = 0.0f) {
		this->x = x;
		this->y = y;
		this->z = z;
	}
	/* Common arithmetic operations */
	Vector operator + (const Vector &v) const {
		return Vector(x + v.x, y + v.y, z + v.z); 
	}
	Vector &operator += (const Vector &v) {
		this->x += v.x;
		this->y += v.y;
		this->z += v.z;
		return *this;
	}
	Vector operator - (const Vector &v) const {
		return Vector(x - v.x, y - v.y, z - v.z); 
	}
	Vector &operator -= (const Vector &v) {
		this->x -= v.x;
		this->y -= v.y;
		this->z -= v.z;
		return *this;
	}
	Vector operator - () const {
		return Vector(-x, -y, -z);
	}
	Vector operator * (float s) const {
		return Vector(x * s, y * s, z * s); 
	}
	Vector operator * (const Vector &v) const {
		return Vector(x * v.x, y * v.y, z * v.z);
	}
	Vector &operator *= (float s) {
		x *= s;
		y *= s;
		z *= s;
		return *this;
	}
	Vector operator / (float s) const {
		return Vector(x / s, y / s, z / s); 
	}
	Vector &operator /= (float s) {
		x /= s;
		y /= s;
		z /= s;
		return *this;
	}
	float operator [] (int i) const {
		if (i <= 2) {
			return (&x)[i];
		}
		return 0;
	}
	float &operator [] (int i) {
		if (i <= 2) {
			return (&x)[i];
		}
		return (&x)[0];
	}
	Vector mult(const Vector &v) const {
		return Vector(x * v.x, y * v.y, z * v.z); 
	}
	Vector& normalize() {
		float s = 1.0f / sqrtf(x*x + y*y + z*z);
		this->x *= s;
		this->y *= s;
		this->z *= s;
		return *this;
	}
	float length() const {
		return sqrt(x*x + y*y + z*z); 
	}
	float lengthSquared() const {
		return x*x + y*y + z*z; 
	}
	float dot(const Vector &b) const { 
		return x * b.x + y * b.y + z * b.z; 
	}
	Vector operator % (const Vector &b) const {
		//return Vector(y * b.z - z * b.y, -z * b.x + x * b.z, -x * b.y + y * b.x);
		float nx = y * b.z - z * b.y;
		float ny = z * b.x - x * b.z;
		float nz = x * b.y - y * b.x;
		return Vector(nx, ny, nz);
	}

	float x, y, z;
};

NAMESPACE_DEBRIS_END
