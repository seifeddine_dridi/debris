#pragma once

#include <core/geometry/ray.h>

#include <vector>

NAMESPACE_DEBRIS_BEGIN

typedef Eigen::Transform<float, 3, Eigen::TransformTraits::Affine, Eigen::DontAlign> Transform4f;

class Transform {
public:
	Transform() {
		transform.setIdentity();
	}
	Transform(const Vector &w) {
		Vector coVec;
		if (fabs(w.x) <= fabs(w.y)) {
			if (fabs(w.x) <= fabs(w.z)) { // yz plane
				coVec = Vector(0, -w.z, w.y);
			} else {
				coVec = Vector(-w.y, w.x, 0); // xy plane
			}
		} else if (fabs(w.y) <= fabs(w.z)) {
			coVec = Vector(-w.z, 0, w.x); // xz plane
		} else {
			coVec = Vector(-w.y, w.x, 0); // xy plane
		}
		coVec.normalize();
		Vector u = w % coVec;
		Vector v = u % w;
		transform.setIdentity();
		transform(0, 0) = u.x, transform(0, 1) = v.x, transform(0, 2) = w.x, transform(0, 3) = 0;
		transform(1, 0) = u.y, transform(1, 1) = v.y, transform(1, 2) = w.y, transform(1, 3) = 0;
		transform(2, 0) = u.z, transform(2, 1) = v.z, transform(2, 2) = w.z, transform(2, 3) = 0;
		transform(3, 0) = 0, transform(3, 1) = 0, transform(3, 2) = 0, transform(3, 3) = 1;
		invTransform = transform.inverse();
	}
	Transform(const Transform4f &transform) : transform(transform), invTransform(transform.inverse()) {}
	Vector operator * (const Vector &v) const {
		Eigen::Vector3f vt = transform * Eigen::Vector3f(v.x, v.y, v.z);
		return Vector(vt.x(), vt.y(), vt.z());
	}
	Ray operator * (const Ray &ray) const {
		Eigen::Vector3f dir = transform.matrix().topLeftCorner<3, 3>() * Eigen::Vector3f(ray.d.x, ray.d.y, ray.d.z);
		return Ray(operator*(ray.o), Vector(dir.x(), dir.y(), dir.z()), ray.maxt, ray.time);
	}
	Transform operator * (const Eigen::AngleAxisf &t) const {
		return Transform(transform * t);
	}
	const Transform4f &getMatrix() const {
		return transform;
	}
	void setMatrix(const Transform4f &transform) {
		this->transform = transform;
		this->invTransform = transform.inverse();
	}
	Transform getInverse() const {
		return Transform(invTransform);
	}
	void translate(const Vector &v) {
		transform(0, 3) += v.x;
		transform(1, 3) += v.y;
		transform(2, 3) += v.z;
		invTransform = transform.inverse();
	}
	Vector getTranslation() const {
		return Vector(transform(0, 3), transform(1, 3), transform(2, 3));
	}
	void setTranslation(const Vector &v) {
		transform(0, 3) = v.x;
		transform(1, 3) = v.y;
		transform(2, 3) = v.z;
		invTransform = transform.inverse();
	}

protected:
	Transform4f transform;
	Transform4f invTransform;
};

class AnimatedTransform : public Transform {
public:
	AnimatedTransform() : Transform(), startTime(0), endTime(0), rotation(Eigen::AngleAxisf(0.0f, Eigen::Vector3f::Zero())) {}
	AnimatedTransform(int startTime, int endTime, const Eigen::AngleAxisf &rotation) :
		Transform(), startTime(startTime), endTime(endTime), rotation(rotation) {}
	AnimatedTransform(const Transform4f &transform) : Transform(transform) {}
	AnimatedTransform(const AnimatedTransform &obj) {
		this->transform = obj.transform;
		this->startTime = obj.startTime;
		this->endTime = obj.endTime;
		this->rotation = obj.rotation;
	}
	int getStartTime() const {
		return startTime;
	}
	int getEndTime() const {
		return endTime;
	}
	const Eigen::AngleAxisf &getRotation() const {
		return rotation;
	}

private:
	int startTime, endTime;
	Eigen::AngleAxisf rotation;
};

class DEBRISCORE_API InterpolatedTransform {
public:
	InterpolatedTransform();
	InterpolatedTransform(const Transform &startTransform);
	InterpolatedTransform(const Transform &startTransform, const AnimatedTransform &animation);
	InterpolatedTransform(const Transform &startTransform, const AnimatedTransform &animation, int startTime, int endTime);
	Transform interpolate(float time) const;
	void interpolate(float time, Transform &start, Transform &end) const;
	const Transform &getEndTransform() const;
	float getNormalizedTimeValue(float time) const;
	int getTimeSpan() const {
		return endTime - startTime;
	}
	AnimatedTransform getAnimation() const {
		return animation;
	}

private:
	Transform startTransform;
	Transform endTransform;
	AnimatedTransform animation;
	int startTime, endTime;
};

NAMESPACE_DEBRIS_END
