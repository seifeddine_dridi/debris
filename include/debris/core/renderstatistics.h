#pragma once

#include <core/debris.h>

NAMESPACE_DEBRIS_BEGIN

class RenderStatistics {
public:
	RenderStatistics(int spp, int nbTiles) : samplesPerPixel(spp), nbTiles(nbTiles), renderingFinished(false), progress(0) {}
	/**
	* Report the percentage of rendering progress
	* @progress int in [0..nbTiles]
	*/
	void setProgress(int progress) {
		this->progress += progress;
	}
	void renderingHasFinished() {
		renderingFinished = true;
	}
	bool hasFinishedRendering() const {
		return renderingFinished || progress == nbTiles;
	}
	void showProgress() const {
		std::cout << "\rRendering progress " << 100.2f * computeProgressPercentage(progress) << "%";
	}

	int samplesPerPixel;
	int nbTiles;
	bool renderingFinished;
	int progress;

private:
	float computeProgressPercentage(int n) const {
		return n / (float)nbTiles;
	}
};

NAMESPACE_DEBRIS_END
