#pragma once

#include <core/spectrum.h>

NAMESPACE_DEBRIS_BEGIN

enum SamplingMeasure {
	AREA, SOLID_ANGLE
};

struct LightSample {
	Vector point;
	Vector dir;
	Vector normal;
	float dist;
};

class Light {
public:
	virtual Vector sampleArea(float u1, float u2, float &pdf) const = 0;
	virtual float sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const = 0;
	virtual bool hasArea() const = 0;
	virtual bool hasInfiniteArea() const {
		return false;
	}
	virtual bool isDirectional() const = 0;
	virtual float getPdf() const {
		return 0.0f;
	}
	virtual XYZColor getEmittance(const Vector &v) const = 0;
};

class DEBRISCORE_API AreaLight : public Light {
public:
	AreaLight(const shared_ptr<Primitive> &primitive): primitive(primitive) {}
	Vector sampleArea(float u1, float u2, float &pdf) const;
	float sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const;
	bool hasArea() const;
	bool isDirectional() const;
	XYZColor getEmittance(const Vector &v) const;

private:
	const shared_ptr<Primitive> primitive;
};

NAMESPACE_DEBRIS_END
