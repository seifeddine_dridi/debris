#pragma once

#include <core/geometry/affinespace.h>
#include <core/film.h>

NAMESPACE_DEBRIS_BEGIN

class Camera {
public:
	Camera(const Transform &local2world, const shared_ptr<Film> &film) : local2world(local2world), film(film),
		shutterOpen(0.0f), shutterClose(0.0f), shutterSpeed(1.0f) {
		Transform4f scale(Eigen::Matrix4f::Identity());
		float aspectRatio = (float)film->imageWidth / film->imageHeight;
		float scaleFactorX, scaleFactorY;
		if (aspectRatio > 1.0f) {
			scaleFactorX = aspectRatio;
			scaleFactorY = 1.0f;
		} else {
			scaleFactorX = 1.0f;
			scaleFactorY = 1.0f / aspectRatio;
		}
		scale.scale(Eigen::Vector3f(scaleFactorX, scaleFactorY, 0.0f));
		Transform4f m(Eigen::Matrix4f::Zero());
		m(0, 0) = 2.0f * film->invWidth;
		m(1, 1) = -2.0f * film->invHeight;
		m(0, 3) = -1.0f;
		m(1, 3) = 1.0f;
		screen2Raster = scale * m;
	}
	virtual Ray project(float x, float y, float u1 = 0, float u2 = 0, float t = 0) const = 0;
	const Transform &getTransform() const {
		return local2world;
	}
	shared_ptr<Film> &getFilm() {
		return film;
	}
	void setShutterOpen(unsigned int shutterOpen) {
		this->shutterOpen = shutterOpen;
	}

	void setShutterClose(unsigned int shutterClose) {
		this->shutterClose = shutterClose;
	}

	void setShutterSpeed(unsigned int shutterSpeed) {
		this->shutterSpeed = shutterSpeed;
	}

	float computeTime(float t) const {
		// Apply time dilation...
		if (!shutterSpeed) {
			return shutterOpen;
		}
		return shutterOpen + t * (shutterClose - shutterOpen) / (float)shutterSpeed;
		//return (shutterOpen + t * (shutterClose - shutterOpen)) / (float)shutterClose;
	}

	AffineSpace affineSpace;
	Transform local2world;
	Transform screen2Raster;
	shared_ptr<Film> film;
	unsigned int shutterOpen, shutterClose;
	unsigned int shutterSpeed;
};

NAMESPACE_DEBRIS_END
