#pragma once

#include <core/geometry/vector.h>

NAMESPACE_DEBRIS_BEGIN

typedef Vector XYZColor;

namespace spectral {
	XYZColor DEBRISCORE_API getXYZResponseValue(float wavelength);
	XYZColor DEBRISCORE_API toXYZ(float wavelength, float energy);
	XYZColor DEBRISCORE_API rgb2xyz(const Vector &rgb);
	XYZColor DEBRISCORE_API xyzTorgb(const XYZColor &xyzColor);
}

#define WAVELENGTH_SAMPLES 81

class DEBRISCORE_API Spectrum {
public:
	Spectrum() {}
	// Uniform spectrum
	Spectrum(float value) {
		for (int i = 0; i < WAVELENGTH_SAMPLES; ++i) {
			values[i] = value;
		}
	}
	Spectrum(float wavelength, float value) {
		float wMin = ((int) wavelength / 5) * 5.0f;
		int b0 = (int) ((wMin - 380.0f) / 400.0f * (WAVELENGTH_SAMPLES - 1.0f));
		values[b0] = value;
	}
	Spectrum operator+(const Spectrum &s) const;
	Spectrum operator/(float s) const;
	Spectrum operator*(float s) const;
	Spectrum operator*(const Spectrum &s) const;
	inline float eval(float wavelength) const {
		return 0; // TODO to be implemented
	}
	Vector toXYZ() const;

	float values[WAVELENGTH_SAMPLES];
	float wavelength;
};

Spectrum DEBRISCORE_API createBellCurve(float wavelength, float scale);

NAMESPACE_DEBRIS_END
