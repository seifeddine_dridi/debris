#pragma once

NAMESPACE_DEBRIS_BEGIN

// Core classes
class AbstractShape;
struct IntersectionRecord;
class Primitive;
class Accelerator;
class Camera;
struct ImageSlider;
struct IntersectionData;
struct Ray;
class Light;
class AreaLight;
class LightIntegrator;
class Scene;
class Spectrum;
class Texture;
class BSDF;
class Shader;
class Film;
class Random;

NAMESPACE_DEBRIS_END
