#pragma once

// Globals constants

const float EPSILON = 0.001f;
#ifdef INFINITY
#undef INFINITY
#endif
const float INFINITY = 1e9f;
