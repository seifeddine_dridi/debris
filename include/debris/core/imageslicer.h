#pragma once

#include <vector>
#include <algorithm>

NAMESPACE_DEBRIS_BEGIN

struct ImageSlice {
	ImageSlice() {}
	ImageSlice(int xstart, int xend, int ystart, int yend, int tileId) {
		this->xstart = xstart;
		this->xend = xend;
		this->ystart = ystart;
		this->yend = yend;
		this->id = tileId;
	}
	int xstart, xend, ystart, yend;
	int id;
};

/**
* This class is responsible for generating a set
* of equally sized tiles for the renderer to consume.
*/
class ImageSlicer {
public:
	ImageSlicer(int blockSize, int imageWidth, int imageHeight) {
		// Calculate the number of blocks
		//this->nbBlocks = imageWidth * imageHeight / (blockSize * blockSize);
		//this->nbBlocks += (imageWidth * imageHeight) % (blockSize * blockSize) != 0 ? 1 : 0;
		int nbBlocks_x = imageWidth / blockSize;
		if (imageWidth % blockSize != 0)
			nbBlocks_x += 1;
		int nbBlocks_y = imageHeight / blockSize;
		if (imageHeight % blockSize != 0)
			nbBlocks_y += 1;
		this->nbBlocks = nbBlocks_x * nbBlocks_y;
		// Generate blocks
		int xPos = 0, yPos = 0;
		for (int i = 0; i < nbBlocks; ++i) {
			blocks.push_back(ImageSlice(xPos, std::min(xPos + blockSize, imageWidth - 1),
				yPos, std::min(yPos + blockSize, imageHeight - 1), i));
			if (xPos + blockSize + 1 < imageWidth)
				xPos += blockSize + 1;
			else {
				xPos = 0;
				yPos += blockSize + 1;
			}
		}
	}
	virtual bool hasNext() const = 0;
	virtual const ImageSlice &getNext() = 0;
	virtual void reset() {}
	int getTilesCount() const {
		return nbBlocks;
	}

protected:
	std::vector<ImageSlice> blocks;
	int nbBlocks;
};

class LinearImageTileOrdering : public ImageSlicer {
public:
	LinearImageTileOrdering(int blockSize, int imageWidth, int imageHeight) : ImageSlicer(blockSize, imageWidth, imageHeight), currentBlock(0) {}
	bool hasNext() const {
		return currentBlock < nbBlocks;
	}
	const ImageSlice &getNext() {
		return blocks[currentBlock++];
	}
	void reset() {
		currentBlock = 0;
	}

private:
	int currentBlock;
};

class RandomImageTileOrdering : public ImageSlicer {
public:
	RandomImageTileOrdering(int blockSize, int imageWidth, int imageHeight) : ImageSlicer(blockSize, imageWidth, imageHeight), currentBlock(0) {
		for (int i = 0; i < nbBlocks; ++i) {
			tilesId.push_back(i);
		}
		std::random_shuffle(tilesId.begin(), tilesId.end());
	}
	bool hasNext() const {
		return currentBlock < nbBlocks;
	}
	const ImageSlice &getNext() {
		return blocks[tilesId[currentBlock++]];
	}
	void reset() {
		currentBlock = 0;
	}

private:
	int currentBlock;
	std::vector<int> tilesId;
};

NAMESPACE_DEBRIS_END
