#pragma once

#include <core/spectrum.h>

#include <OpenImageIO/imagebuf.h>

NAMESPACE_DEBRIS_BEGIN

class Texture {
public:
	virtual XYZColor getColor(float u, float v) const = 0;
};

class DEBRISCORE_API UniformTexture : public Texture {
public:
	UniformTexture(const Vector &rgb);
	UniformTexture(float wavelength, float value);
	XYZColor getColor(float u, float v) const;

private:
	XYZColor color;
};

class DEBRISCORE_API CheckerBoard : public Texture {
public:
	CheckerBoard(float sizex, float sizey);
	XYZColor getColor(float u, float v) const;

private:
	float sizex, sizey;
};

class DEBRISCORE_API ImageTexture : public Texture {
public:
	ImageTexture(const std::string &imagesrc);
	~ImageTexture();
	XYZColor getColor(float u, float v) const;

private:
	OIIO::ImageBuf imageBuf;
	OIIO::ImageSpec imageSpec;
};

NAMESPACE_DEBRIS_END
