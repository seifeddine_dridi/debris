#pragma once

#include <core/renderer.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API DefaultRenderer : public Renderer {
public:
	DefaultRenderer(const shared_ptr<LightIntegrator> &integrator);
	void renderFrame(const shared_ptr<Scene> &scene, float time, int spp, const ImageSlice &tile);

private:
	shared_ptr<LightIntegrator> integrator;
};

NAMESPACE_DEBRIS_END
