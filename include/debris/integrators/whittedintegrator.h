#pragma once

#include <core/lightintegrator.h>
#include <core/random.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API WhittedIntegrator : public LightIntegrator {
public:
	WhittedIntegrator(int maxBounces): m_maxBounces(maxBounces) {}
	XYZColor getRadiance(const Ray &ray, const shared_ptr<Scene> &scene, float wavelength, Random &rng) const;

private:
	XYZColor getRadiance(const Ray &ray, const shared_ptr<Scene> &scene, float wavelength, int depth, Random &rng) const;
	XYZColor computeDirectLighting(const Ray &ray, const IntersectionRecord &info, const shared_ptr<Scene> &scene, float wavelength, Random &rng) const;

	const int m_maxBounces;
};

NAMESPACE_DEBRIS_END
