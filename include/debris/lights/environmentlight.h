#pragma once

#include <core/light.h>
#include <core/texture.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API EnvironmentLight : public Light {
public:
	EnvironmentLight(const char *imagePath);
	Vector sampleArea(float u1, float u2, float &pdf) const {
		throw DebrisException("An environment light cannot be area-sampled!");
	}
	float sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const;
	bool hasArea() const;
	bool hasInfiniteArea() const {
		return true;
	}
	bool isDirectional() const;
	XYZColor getEmittance(const Vector &v) const;

private:
	ImageTexture hdrImage;
};

NAMESPACE_DEBRIS_END
