#pragma once

#include <core/light.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API PointLight : public Light {
public:
	PointLight(const Vector &position, float intensity);
	Vector sampleArea(float u1, float u2, float &pdf) const;
	float sampleLight(const Vector &point, const Vector &normal, float u1, float u2, float &pdf, Vector &dir) const;
	bool hasArea() const;
	bool isDirectional() const;
	XYZColor getEmittance(const Vector &v) const;

private:
	Vector position;
	float intensity;
};

NAMESPACE_DEBRIS_END
