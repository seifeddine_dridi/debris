#pragma once

#include <core/camera.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API OrthographicCamera : public Camera {
public:
	OrthographicCamera(const Transform &loca2world, const shared_ptr<Film> &film, float zoom);
	Ray project(float x, float y, float u1 = 0, float u2 = 0, float t = 0) const;

private:
	float zoom;
};

NAMESPACE_DEBRIS_END
