#pragma once

#include <core/camera.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API PerspectiveCamera : public Camera {
public:
	PerspectiveCamera(const Transform &loca2world, const shared_ptr<Film> &film, float lensRadius, float focalDistance, float focusDistance = INFINITY);
	Ray project(float x, float y, float u1 = 0, float u2 = 0, float t = 0) const;

private:
	float lensRadius;
	float focalDistance, focusDistance;
};

NAMESPACE_DEBRIS_END
