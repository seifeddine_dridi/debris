#pragma once

#include <core/camera.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API PanoramicCamera : public Camera {
public:
	PanoramicCamera(const Transform &loca2world, const shared_ptr<Film> &film, float fieldOfView, float focalDistance);
	Ray project(float x, float y, float u1 = 0, float u2 = 0, float t = 0) const;

private:
	float fieldOfView;
	float focalDistance;
};

NAMESPACE_DEBRIS_END
