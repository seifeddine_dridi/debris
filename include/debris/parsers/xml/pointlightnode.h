#pragma once

#include <lightnode.h>

#include <lights/pointlight.h>

NAMESPACE_DEBRIS_BEGIN

class PointLightNode : public LightNode {
public:
	PointLightNode(Node *parent) : LightNode(parent), intensity(1.0f) {}
	std::wstring getName() const {
		return L"point-light";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"intensity") != nullptr) {
			XMLFloat node(attrs.getValue(L"intensity"));
			intensity = (float) node.getValue();
		}
		if (attrs.getValue(L"position") != nullptr) {
			parseFloat3(attrs.getValue(L"position"), position);
		}
	}
	void endNode() {
		instance = boost::shared_ptr<Light>(new PointLight(position, intensity));
	}

	float intensity;
	Vector position;
};

NAMESPACE_DEBRIS_END
