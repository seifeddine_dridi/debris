#pragma once

#include <lightnode.h>

#include <lights/environmentlight.h>

NAMESPACE_DEBRIS_BEGIN

class EnvironmentLightNode : public LightNode {
public:
	EnvironmentLightNode(Node *parent) : LightNode(parent) {}
	std::wstring getName() const {
		return L"environment-light";
	}
	void bind(const Attributes& attrs) {
		char *strBuffer = XMLString::transcode(attrs.getValue(L"src"));
		imagePath = std::string(strBuffer);
		XMLString::release(&strBuffer);
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::LIGHT;
	}
	void endNode() {
		instance = boost::shared_ptr<Light>(new EnvironmentLight(imagePath.c_str()));
	}

	std::string imagePath;
};

NAMESPACE_DEBRIS_END
