#pragma once

#include <cameranode.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API PanoramicCameraNode : public CameraNode {
public:
	PanoramicCameraNode(Node *parent) : CameraNode(parent), focalDistance(INFINITY) {}
	std::wstring getName() const {
		return L"panoramic-camera";
	}
	void bind(const Attributes& attrs) {
		CameraNode::bind(attrs);
		if (attrs.getValue(L"fov") != nullptr) {
			XMLFloat node(attrs.getValue(L"fov"));
			fieldOfView = (float)node.getValue();
		}
		if (attrs.getValue(L"focal-distance") != nullptr) {
			XMLFloat node(attrs.getValue(L"focal-distance"));
			focalDistance = (float)node.getValue();
		}
	}
	Camera *instantiateCamera() const;

private:
	float fieldOfView, focalDistance;
};

NAMESPACE_DEBRIS_END
