#pragma once

#include <stringutils.h>

#include <string>
#include <list>

#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax2/Attributes.hpp>

NAMESPACE_DEBRIS_BEGIN

enum NODE_TYPE {
	NONE,
	SCENE,
	SHAPE,
	LIGHT,
	CAMERA,
	TRANSFORM,
	ANIMATED_TRANSFORM,
	TRANSLATE,
	ROTATE,
	LOOKAT,
	TEXTURE,
	SHADER
};

class Node {
public:
	Node(Node *parent = nullptr) : parent(parent) {}
	~Node() {
		// Remove allocated children
		/*for (std::list<const Node*>::iterator iter = children.begin(); iter != children.end(); iter++) {
			delete (*iter);
		}*/
		while (children.size() > 0) {
			const Node *node = children.back();
			children.pop_back();
			delete node;
		}
	}
	virtual void startNode() {}
	virtual void endNode() {}
	virtual bool validateChild(const std::wstring &childName) const {
		return true;
	}
	virtual void addChild(const Node *child) {
		children.push_back(child);
	}
	const Node *getChild(const std::wstring &name) const {
		std::list<const Node*>::const_iterator childIter;
		for (childIter = children.begin(); childIter != children.end(); childIter++) {
			if ((*childIter)->getName() == name)
				return (*childIter);
		}
		return nullptr;
	}
	std::list<const Node *> getChildren(const std::wstring &name) const {
		std::list<const Node*> childList;
		std::list<const Node*>::const_iterator childIter;
		for (childIter = children.begin(); childIter != children.end(); childIter++) {
			if ((*childIter)->getName() == name)
				childList.push_back(*childIter);
		}
		return childList;
	}
	const std::list<const Node*> &getChildren() const {
		return children;
	}
	virtual std::wstring getName() const = 0;
	virtual void bind(const Attributes& attrs) {}
	Node *getParent() const {
		return parent;
	}
	virtual NODE_TYPE getType() const {
		return NODE_TYPE::NONE;
	}

protected:
	Node *parent;
	std::list<const Node*> children;
};


NAMESPACE_DEBRIS_END
