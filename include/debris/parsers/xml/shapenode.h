#pragma once

#include <animatedtransformnode.h>
#include <shadernode.h>

#include <core/primitive.h>

NAMESPACE_DEBRIS_BEGIN

class ShapeNode : public Node {
public:
	ShapeNode(Node *parent) : Node(parent) {}
	NODE_TYPE getType() const {
		return NODE_TYPE::SHAPE;
	}
	void addChild(const Node *child) {
		Node::addChild(child);
		if (child->getType() == NODE_TYPE::SHADER) {
			shader = ((ShaderNode*)child)->instance;
		} else if (child->getType() == NODE_TYPE::TRANSFORM) {
			transform = *((TransformNode*)child)->instance;
		} else if (child->getType() == NODE_TYPE::ANIMATED_TRANSFORM) {
			AnimatedTransformNode *animatedTrans = (AnimatedTransformNode*)child;
			animatedTransform = animatedTrans->instance;
		}
	}
	virtual void createShape() = 0;
	void endNode() {
		createShape();
		if (!shader.use_count()) {
			std::wstring message(L"shader element is missing from element <" + getName() + L">");
			throw SAXException(message.c_str());
		}
		if (animatedTransform.use_count()) {
			AnimatedTransform *animation = (AnimatedTransform*)animatedTransform.get();
			shape->setAnimation(*animation);
		}
		instance = boost::shared_ptr<Primitive>(new Primitive(shape, shader));
	}
	Scene *getScene() const;

	boost::shared_ptr<Primitive> instance;
	boost::shared_ptr<AbstractShape> shape;
	boost::shared_ptr<Shader> shader;
	Transform transform;
	boost::shared_ptr<Transform> animatedTransform;
};

NAMESPACE_DEBRIS_END
