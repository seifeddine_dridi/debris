#pragma once

#include <translatenode.h>
#include <rotatenode.h>

#include <core/geometry/geometry.h>

NAMESPACE_DEBRIS_BEGIN

class TransformNode : public Node {
public:
	TransformNode(Node *parent) : Node(parent), rotation(Eigen::AngleAxisf(0.0f, Eigen::Vector3f::Zero())) {
	}
	std::wstring getName() const {
		return L"transform";
	}
	virtual void endNode() {
		if (getChildren().size() == 0) {
			throw SAXException(L"transform node must have at least one child!");
		}
		Transform4f transform;
		transform.setIdentity();
		Eigen::Vector3f translation = Eigen::Vector3f::Zero();
		std::list<const Node*> tNodes = getChildren(L"translate");
		for (const Node *node : tNodes) {
			const TranslateNode *t = (const TranslateNode*)node;
			translation += Eigen::Vector3f(t->x, t->y, t->z);
		}
		transform(0, 3) = translation.x(), transform(1, 3) = translation.y(), transform(2, 3) = translation.z();
		transform(3, 0) = 0.0f, transform(3, 1) = 0.0f, transform(3, 2) = 0.0f, transform(3, 3) = 1.0f;
		std::list<const Node*> rotNodes = getChildren(L"rotate");
		int counter = 0;
		for (std::list<const Node*>::reverse_iterator it = rotNodes.rbegin(); it != rotNodes.rend(); ++it) {
			const RotateNode *rotationNode = (const RotateNode*)*it;
			// Generate 4x4 matrix from (angle, axis) pair
			float radAngle = degreeToRadians(rotationNode->angle);
			Eigen::AngleAxisf aa(radAngle, Eigen::Vector3f(rotationNode->x, rotationNode->y, rotationNode->z));
			// M = Translation * Rotation
			transform = transform * aa;
			if (!counter) {
				rotation = aa;
			}
			else {
				rotation = rotation * aa;
			}
			++counter;
		}
		instance = boost::shared_ptr<Transform>(createInstance());
		instance->setMatrix(transform);
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::TRANSFORM;
	}

	boost::shared_ptr<Transform> instance;

protected:
	virtual Transform* createInstance() const {
		return new Transform();
	}

	Eigen::AngleAxisf rotation;
};

NAMESPACE_DEBRIS_END