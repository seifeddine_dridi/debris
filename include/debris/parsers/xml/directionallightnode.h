#pragma once

#include <lightnode.h>

#include <lights/directionallight.h>

NAMESPACE_DEBRIS_BEGIN

class DirectionalLightNode : public LightNode {
public:
	DirectionalLightNode(Node *parent) : LightNode(parent), intensity(1.0f) {}
	std::wstring getName() const {
		return L"directional-light";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"intensity") != nullptr) {
			XMLFloat node(attrs.getValue(L"intensity"));
			intensity = (float)node.getValue();
		}
		if (attrs.getValue(L"direction") != nullptr) {
			parseFloat3(attrs.getValue(L"direction"), direction);
		}
	}
	void endNode() {
		instance = boost::shared_ptr<Light>(new DirectionalLight(direction, intensity));
	}

	float intensity;
	Vector direction;
};

NAMESPACE_DEBRIS_END
