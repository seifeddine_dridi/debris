#pragma once

#include <cameranode.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API PerspectiveCameraNode : public CameraNode {
public:
	PerspectiveCameraNode(Node *parent) : CameraNode(parent), lensRadius(0.0f), focalDistance(1.0f), focusDistance(INFINITY) {}
	std::wstring getName() const {
		return L"perspective-camera";
	}
	void bind(const Attributes& attrs) {
		CameraNode::bind(attrs);
		if (attrs.getValue(L"lens-radius") != nullptr) {
			XMLFloat node(attrs.getValue(L"lens-radius"));
			lensRadius = (float) node.getValue();
		}
		if (attrs.getValue(L"focal-distance") != nullptr) {
			XMLFloat node(attrs.getValue(L"focal-distance"));
			focalDistance = (float) node.getValue();
		}
		if (attrs.getValue(L"focus-distance") != nullptr) {
			XMLFloat node(attrs.getValue(L"focus-distance"));
			focusDistance = (float)node.getValue();
		}
	}
	Camera *instantiateCamera() const;

private:
	float lensRadius, focalDistance, focusDistance;
};

NAMESPACE_DEBRIS_END
