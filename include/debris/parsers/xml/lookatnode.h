#pragma once

#include <node.h>

#include <core/geometry/geometry.h>

NAMESPACE_DEBRIS_BEGIN

class LookAtNode : public Node {
public:
	LookAtNode(Node *parent) : Node(parent) {}
	std::wstring getName() const {
		return L"lookat";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"origin") != nullptr) {
			parseFloat3(attrs.getValue(L"origin"), origin);
		}
		if (attrs.getValue(L"up") != nullptr) {
			parseFloat3(attrs.getValue(L"up"), up);
		}
		if (attrs.getValue(L"direction") != nullptr) {
			parseFloat3(attrs.getValue(L"direction"), direction);
		}
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::LOOKAT;
	}
	Transform4f getTransform() const {
		Transform4f transform;
		transform.setIdentity();
		transform(0, 3) = origin.x;
		transform(1, 3) = origin.y;
		transform(2, 3) = origin.z;
		Vector xAxis = direction % up;
		transform(0, 0) = xAxis.x, transform(1, 0) = xAxis.y, transform(2, 0) = xAxis.z;
		transform(0, 1) = up.x, transform(1, 1) = up.y, transform(2, 1) = up.z;
		transform(0, 2) = direction.x, transform(1, 2) = direction.y, transform(2, 2) = direction.z;
		// Set bottom row of the matrix
		transform(3, 0) = 0.0f;
		transform(3, 1) = 0.0f;
		transform(3, 2) = 0.0f;
		transform(3, 3) = 1.0f;
		return transform;
	}

	Vector origin, up, direction;
};

NAMESPACE_DEBRIS_END
