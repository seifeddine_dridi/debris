#pragma once

#include <shapenode.h>
#include <cameranode.h>
#include <lightnode.h>

#include <core/scene.h>

NAMESPACE_DEBRIS_BEGIN

class SceneNode : public Node {
public:
	std::wstring getName() const {
		return L"scene";
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::SCENE;
	}
	void addChild(const Node *child) {
		Node::addChild(child);
		if (child->getType() == NODE_TYPE::CAMERA) {
			camera = ((CameraNode*)child)->instance;
		} else if (child->getType() == NODE_TYPE::SHAPE) {
			boost::shared_ptr<Primitive> primitive = ((ShapeNode*)child)->instance;
			primitives.push_back(primitive);
			if (primitive->isEmitter()) {
				lights.push_back(shared_ptr<Light>(new AreaLight(primitive)));
			}
		} else if (child->getType() == NODE_TYPE::LIGHT) {
			lights.push_back(((LightNode*) child)->instance);
		}
	}
	void endNode();

	std::vector<boost::shared_ptr<Primitive>> primitives;
	std::vector<boost::shared_ptr<Light>> lights;
	boost::shared_ptr<Camera> camera;
	boost::shared_ptr<Scene> scene;
};

NAMESPACE_DEBRIS_END
