#pragma once

#include <shapenode.h>

#include <shapes/sphere.h>

NAMESPACE_DEBRIS_BEGIN

class SphereNode : public ShapeNode {
public:
	SphereNode(Node *parent) : ShapeNode(parent) {}
	std::wstring getName() const {
		return L"sphere";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"radius") != nullptr) {
			XMLFloat node(attrs.getValue(L"radius"));
			radius = (float) node.getValue();
		}
		parseFloat3(attrs.getValue(L"position"), position);
	}
	void createShape() {
		shape = boost::shared_ptr<AbstractShape>(new Sphere(position, radius, transform));
	}

	float radius;
	Vector position;
};

NAMESPACE_DEBRIS_END
