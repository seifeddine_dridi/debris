#pragma once

#include <shapenode.h>

#include <shapes/trianglemesh.h>

NAMESPACE_DEBRIS_BEGIN

class  DEBRISCORE_API TriangleMeshNode : public ShapeNode {
public:
	TriangleMeshNode(Node *parent) : ShapeNode(parent) {}
	std::wstring getName() const {
		return L"triangle-mesh";
	}
	void bind(const Attributes& attrs) {
		char *srcStr = XMLString::transcode(attrs.getValue(L"src"));
		this->src = std::string(srcStr);
		XMLString::release(&srcStr);
	}
	void createShape();

private:
	std::string src;
};

NAMESPACE_DEBRIS_END
