#pragma once

#include <shapenode.h>

#include <shapes/triangle.h>

NAMESPACE_DEBRIS_BEGIN

class TriangleNode : public ShapeNode {
public:
	TriangleNode(Node *parent) : ShapeNode(parent), hasUVCoords(true) {}
	std::wstring getName() const {
		return L"triangle";
	}
	void bind(const Attributes& attrs) {
		parseFloat3(attrs.getValue(L"v1"), vertices[0]);
		parseFloat3(attrs.getValue(L"v2"), vertices[1]);
		parseFloat3(attrs.getValue(L"v3"), vertices[2]);
		const wchar_t *attrName[] = { { L"uv1" }, { L"uv2" }, { L"uv3" } };
		for (int i = 0; i < 3; ++i) {
			if (attrs.getValue(attrName[i]) != nullptr) {
				parseFloat2(attrs.getValue(attrName[i]), uvs[i]);
			} else {
				hasUVCoords = false;
				break;
			}
		}
	}
	void createShape() {
		if (hasUVCoords) {
			shape = boost::shared_ptr<AbstractShape>(new Triangle(transform, vertices, uvs));
		}
		else {
			shape = boost::shared_ptr<AbstractShape>(new Triangle(transform, vertices));
		}
	}

private:
	Vector vertices[3];
	Eigen::Vector2f uvs[3];
	bool hasUVCoords;
};

NAMESPACE_DEBRIS_END
