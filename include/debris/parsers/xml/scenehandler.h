#pragma once

#include <xmlelementmapping.h>
#include <scenenode.h>

#include <iostream>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>

NAMESPACE_DEBRIS_BEGIN

using namespace xercesc;

class DEBRISCORE_API SceneHandler : public DefaultHandler {
public:
	SceneHandler();
	~SceneHandler();
	void startElement(const XMLCh* const uri, const XMLCh* const localname,
		const XMLCh* const qname, const Attributes& attrs);
	void endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname);
	void fatalError(const SAXParseException& exception);
	void error(const SAXParseException& exception);
	SceneNode *getSceneNode() const;

private:
	Node *rootNode = nullptr;
	Node *currentNode = nullptr;
	XMLElementMapping elementMapping;
};

DEBRISCORE_API SAX2XMLReader *createConfiguredSAXParser();

NAMESPACE_DEBRIS_END
