#pragma once

#include <node.h>
#include <colorfunction.h>

#include <boost/algorithm/string/predicate.hpp>

NAMESPACE_DEBRIS_BEGIN

class TextureNode : public Node {
public:
	TextureNode(Node *parent) : Node(parent), colorFunc(nullptr) {}
	~TextureNode() {
		if (colorFunc) {
			delete colorFunc;
		}
	}
	std::wstring getName() const {
		return L"texture";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"color") != nullptr) {
			char *strBuffer = XMLString::transcode(attrs.getValue(L"color"));
			std::string colorStr(strBuffer);
			if (colorStr.empty()) {
				throw SAXException(L"Empty attribute \"color\" in element <texture>");
			}
			if (boost::starts_with(colorStr, "rgb(") && boost::ends_with(colorStr, ")")) {
				colorFunc = new RGBColorFunction();
			} else if (boost::starts_with(colorStr, "checker(") && boost::ends_with(colorStr, ")")) {
				colorFunc = new CheckerColorFunction();
			} else {
				char target[1024] = "\0";
				XMLString::catString(target, (std::string("Unrecognized color function: ") + strBuffer).c_str());
				throw SAXException(target);
			}
			colorFunc->parse(colorStr);
			XMLString::release(&strBuffer);
		} else if (attrs.getValue(L"src") != nullptr) {
			char *strBuffer = XMLString::transcode(attrs.getValue(L"src"));
			texturePath = std::string(strBuffer);
			XMLString::release(&strBuffer);
		} else {
			throw SAXException(L"Attributes src or color are missing from element <texture>");
		}
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::TEXTURE;
	}
	void endNode() {
		if (colorFunc) {
			instance = boost::shared_ptr<Texture>(colorFunc->getInstance());
		} else {
			instance = boost::shared_ptr<Texture>(new ImageTexture(texturePath));
		}
	}

	ColorFunction *colorFunc;
	std::string texturePath;
	boost::shared_ptr<Texture> instance;
};

NAMESPACE_DEBRIS_END
