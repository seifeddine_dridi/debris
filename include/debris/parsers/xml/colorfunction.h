#pragma once

#include <core/texture.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API ColorFunction {
public:
	virtual void parse(const std::string &colorStr) = 0;
	virtual Texture *getInstance() const = 0;
	virtual std::string getName() const = 0;

protected:
	void parse(const std::string &colorStr, float *params, int N);
};

class RGBColorFunction : public ColorFunction {
public:
	void parse(const std::string &colorStr) {
		ColorFunction::parse(colorStr, params, 3);
	}
	Texture *getInstance() const {
		return new UniformTexture(Vector(params[0], params[1], params[2]));
	}
	std::string getName() const {
		return "rgb";
	}

private:
	float params[3];
};

class CheckerColorFunction : public ColorFunction {
public:
	void parse(const std::string &colorStr) {
		ColorFunction::parse(colorStr, params, 2);
	}
	Texture *getInstance() const {
		return new CheckerBoard(params[0], params[1]);
	}
	std::string getName() const {
		return "checker";
	}

private:
	float params[2];
};

NAMESPACE_DEBRIS_END
