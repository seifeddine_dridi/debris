#pragma once

#include <lookatnode.h>
#include <transformnode.h>
#include <core/camera.h>

NAMESPACE_DEBRIS_BEGIN

class CameraNode : public Node {
public:
	CameraNode(Node *parent) : Node(parent), shutterOpen(0), shutterClose(0), shutterSpeed(0) {}
	NODE_TYPE getType() const {
		return NODE_TYPE::CAMERA;
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"width") != nullptr) {
			XMLFloat node(attrs.getValue(L"width"));
			width = (float)node.getValue();
		}
		if (attrs.getValue(L"height") != nullptr) {
			XMLFloat node(attrs.getValue(L"height"));
			height = (float)node.getValue();
		}
		if (attrs.getValue(L"shutter-open") != nullptr) {
			XMLBigInteger node(attrs.getValue(L"shutter-open"));
			shutterOpen = node.intValue();
		}
		if (attrs.getValue(L"shutter-close") != nullptr) {
			XMLBigInteger node(attrs.getValue(L"shutter-close"));
			shutterClose = node.intValue();
			if (shutterClose < shutterOpen) {
				std::swap(shutterOpen, shutterClose);
			}
		}
		if (attrs.getValue(L"shutter-speed") != nullptr) {
			XMLBigInteger node(attrs.getValue(L"shutter-speed"));
			shutterSpeed = node.intValue();
		}
	}
	void addChild(const Node *child) {
		Node::addChild(child);
		if (child->getType() == NODE_TYPE::LOOKAT) {
			lookAt = ((LookAtNode*)child)->getTransform();
		}
		if (child->getType() == NODE_TYPE::TRANSFORM) {
			transform = *((TransformNode*)child)->instance;
		}
	}
	void endNode() {
		transform.setMatrix(lookAt * transform.getMatrix());
		Camera *camera = instantiateCamera();
		camera->setShutterOpen(shutterOpen);
		camera->setShutterClose(shutterClose);
		camera->setShutterSpeed(shutterSpeed);
		instance = boost::shared_ptr<Camera>(camera);
	}

	boost::shared_ptr<Camera> instance;
	Transform4f lookAt;
	Transform transform;
	float width, height;
	unsigned int shutterOpen, shutterClose;
	unsigned int shutterSpeed;

protected:
	virtual Camera *instantiateCamera() const = 0;
};

NAMESPACE_DEBRIS_END
