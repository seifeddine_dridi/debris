#pragma once

#include <cameranode.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API OrthographicCameraNode : public CameraNode {
public:
	OrthographicCameraNode(Node *parent) : CameraNode(parent) {}
	std::wstring getName() const {
		return L"orthographic-camera";
	}
	void bind(const Attributes& attrs) {
		CameraNode::bind(attrs);
		if (attrs.getValue(L"zoom") != nullptr) {
			XMLFloat node(attrs.getValue(L"zoom"));
			zoom = (float)node.getValue();
		}
	}
	Camera *instantiateCamera() const;

private:
	float zoom;
};

NAMESPACE_DEBRIS_END
