#pragma once

#include <texturenode.h>

NAMESPACE_DEBRIS_BEGIN

class Shader;

class ShaderNode : public Node {
public:
	ShaderNode(Node *parent) : Node(parent) {}
	NODE_TYPE getType() const {
		return NODE_TYPE::SHADER;
	}
	void addChild(const Node *child) {
		Node::addChild(child);
		if (child->getType() == NODE_TYPE::TEXTURE) {
			texture = ((TextureNode*) child)->instance;
		}
	}

	boost::shared_ptr<Shader> instance;
	boost::shared_ptr<Texture> texture;
};

NAMESPACE_DEBRIS_END
