#pragma once

#include <shadernode.h>

#include <shaders/mirrorshader.h>

NAMESPACE_DEBRIS_BEGIN

class MirrorShaderNode : public ShaderNode {
public:
	MirrorShaderNode(Node *parent) : ShaderNode(parent), kr(0.8f) {}
	std::wstring getName() const {
		return L"mirror-shader";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"kr") != nullptr) {
			XMLFloat node(attrs.getValue(L"kr"));
			kr = (float)node.getValue();
		}
		if (attrs.getValue(L"emittance") != nullptr) {
			XMLFloat node(attrs.getValue(L"emittance"));
			emittance = (float)node.getValue();
		}
	}
	void endNode() {
		if (!texture.use_count()) {
			throw new SAXException("texture element is missing from <mirror-shader>");
		}
		instance = boost::shared_ptr<Shader>(new MirrorShader(texture, kr, emittance));
	}

	float kr, emittance;
};

NAMESPACE_DEBRIS_END
