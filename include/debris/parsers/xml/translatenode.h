#pragma once

#include <node.h>

NAMESPACE_DEBRIS_BEGIN

class TranslateNode : public Node {
public:
	TranslateNode(Node *parent) : Node(parent) {}
	std::wstring getName() const {
		return L"translate";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"x") != nullptr) {
			XMLFloat node(attrs.getValue(L"x"));
			x = (float) node.getValue();
		}
		if (attrs.getValue(L"y") != nullptr) {
			XMLFloat node(attrs.getValue(L"y"));
			y = (float) node.getValue();
		}
		if (attrs.getValue(L"z") != nullptr) {
			XMLFloat node(attrs.getValue(L"z"));
			z = (float) node.getValue();
		}
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::TRANSLATE;
	}

	float x, y, z;
};

NAMESPACE_DEBRIS_END
