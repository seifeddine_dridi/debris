#pragma once

#include <shadernode.h>

#include <shaders/diffuseshader.h>

NAMESPACE_DEBRIS_BEGIN

class DiffuseShaderNode : public ShaderNode {
public:
	DiffuseShaderNode(Node *parent) : ShaderNode(parent), kd(0.8f) {}
	std::wstring getName() const {
		return L"diffuse-shader";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"kd") != nullptr) {
			XMLFloat node(attrs.getValue(L"kd"));
			kd = (float) node.getValue();
		}
		if (attrs.getValue(L"emittance") != nullptr) {
			XMLFloat node(attrs.getValue(L"emittance"));
			emittance = (float) node.getValue();
		}
	}
	void endNode() {
		if (!texture.use_count()) {
			throw new SAXException("texture element is missing from <diffuse-shader>");
		}
		instance = boost::shared_ptr<Shader>(new DiffuseShader(texture, kd, emittance));
	}

	float kd, emittance;
};

NAMESPACE_DEBRIS_END
