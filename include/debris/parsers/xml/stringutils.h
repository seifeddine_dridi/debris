#pragma once

#include <core/debris.h>
#include <core/geometry/vector.h>

#include <xercesc/util/XMLStringTokenizer.hpp>
#include <xercesc/util/XMLFloat.hpp>

using namespace xercesc;

NAMESPACE_DEBRIS_BEGIN

inline void parseFloat3(const XMLCh *str, Vector &result) {
	XMLStringTokenizer originStr(str);
	for (int i = 0; i < 3 && originStr.hasMoreTokens(); ++i) {
		XMLFloat val(originStr.nextToken());
		result[i] = val.getValue();
	}
}

inline void parseFloat2(const XMLCh *str, Eigen::Vector2f &result) {
	XMLStringTokenizer originStr(str);
	for (int i = 0; i < 2 && originStr.hasMoreTokens(); ++i) {
		XMLFloat val(originStr.nextToken());
		result[i] = val.getValue();
	}
}

NAMESPACE_DEBRIS_END
