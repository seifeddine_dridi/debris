#pragma once

#include <node.h>

#include <xercesc/util/XMLBigInteger.hpp>

NAMESPACE_DEBRIS_BEGIN

class RotateNode : public Node {
public:
	RotateNode(Node *parent) : Node(parent), x(0), y(0), z(0) {}
	std::wstring getName() const {
		return L"rotate";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"x") != nullptr) {
			XMLBigInteger node(attrs.getValue(L"x"));
			x = ((int) node.intValue()) != 0 ? 1 : 0;
		} else if (attrs.getValue(L"y") != nullptr) {
			XMLBigInteger  node(attrs.getValue(L"y"));
			y = ((int)node.intValue()) != 0 ? 1 : 0;
		} else if (attrs.getValue(L"z") != nullptr) {
			XMLBigInteger node(attrs.getValue(L"z"));
			z = ((int)node.intValue()) != 0 ? 1 : 0;
		}
		if (attrs.getValue(L"angle") != nullptr) {
			XMLFloat node(attrs.getValue(L"angle"));
			angle = (float) node.getValue();
		}
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::ROTATE;
	}

	int x, y, z;
	float angle;
};

NAMESPACE_DEBRIS_END