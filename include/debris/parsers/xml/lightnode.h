#pragma once

#include <node.h>

NAMESPACE_DEBRIS_BEGIN

class Light;

class LightNode : public Node {
public:
	LightNode(Node *parent) : Node(parent) {}
	NODE_TYPE getType() const {
		return NODE_TYPE::LIGHT;
	}

	boost::shared_ptr<Light> instance;
};

NAMESPACE_DEBRIS_END
