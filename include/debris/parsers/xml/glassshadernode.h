#pragma once

#include <shadernode.h>

#include <shaders/glassshader.h>

NAMESPACE_DEBRIS_BEGIN

class GlassShaderNode : public ShaderNode {
public:
	GlassShaderNode(Node *parent) : ShaderNode(parent), iorOut(1.0f), iorIn(1.5f) {}
	std::wstring getName() const {
		return L"glass-shader";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"ior-out") != nullptr) {
			XMLFloat node(attrs.getValue(L"ior-out"));
			iorOut = (float)node.getValue();
		}
		if (attrs.getValue(L"ior-in") != nullptr) {
			XMLFloat node(attrs.getValue(L"ior-in"));
			iorIn = (float)node.getValue();
		}
	}
	void endNode() {
		if (!texture.use_count()) {
			throw new SAXException("texture element is missing from <glass-shader>");
		}
		instance = boost::shared_ptr<Shader>(new GlassShader(texture, iorOut, iorIn));
	}

	float iorOut, iorIn;
};

NAMESPACE_DEBRIS_END
