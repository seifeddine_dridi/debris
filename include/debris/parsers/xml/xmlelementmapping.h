#pragma once

#include <node.h>

#include <map>

NAMESPACE_DEBRIS_BEGIN

class NodeMaker {
public:
	virtual Node *createNode(Node *parent) const = 0;
};

class DEBRISCORE_API XMLElementMapping {
public:
	XMLElementMapping();
	Node *createNode(const std::wstring &name, Node *parent);

private:
	std::map<const std::wstring, const NodeMaker*> elementMapping;
};

NAMESPACE_DEBRIS_END
