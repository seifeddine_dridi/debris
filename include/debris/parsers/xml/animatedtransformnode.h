#pragma once

#include <transformnode.h>

NAMESPACE_DEBRIS_BEGIN

class AnimatedTransformNode : public TransformNode {
public:
	AnimatedTransformNode(Node *parent) : TransformNode(parent) {}
	std::wstring getName() const {
		return L"animated-transform";
	}
	void bind(const Attributes& attrs) {
		if (attrs.getValue(L"start-time") != nullptr) {
			XMLBigInteger node(attrs.getValue(L"start-time"));
			startTime = (int)node.intValue();
		}
		if (attrs.getValue(L"end-time") != nullptr) {
			XMLBigInteger node(attrs.getValue(L"end-time"));
			endTime = (int)node.intValue();
		}
		if (startTime > endTime) {
			std::swap(startTime, endTime);
		}
	}
	void endNode() {
		TransformNode::endNode();
	}
	NODE_TYPE getType() const {
		return NODE_TYPE::ANIMATED_TRANSFORM;
	}
	Transform* createInstance() const {
		return new AnimatedTransform(startTime, endTime, rotation);
	}

	int startTime, endTime;
};

NAMESPACE_DEBRIS_END