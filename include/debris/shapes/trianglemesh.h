#pragma once

#include <core/geometry/abstractshape.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API TriangleMesh : public AbstractShape {
public:
	TriangleMesh(const Transform &transform, const Vector *vertices, const int *triangles, float *uvs, int nbVertices, int nbTriangles);
	~TriangleMesh();
	bool intersect(RTCRay& ray) const;
	bool intersectP(RTCRay& ray) const;
	void postIntersect(const RTCRay &ray, DifferentialGeometry &dg) const;
	bool isInside(const Vector &p) const;
	Vector sampleArea(float u1, float u2, float &pdf) const;
	Vector getNormal(const Vector &point) const;
	void registerShape(RTCScene &scene) const;
	void updateShape(const RTCScene &scene, float time) const;

private:
	Vector *vertices;
	float *uvs;
	int *triangles;
	int nbTriangles, nbVertices;
	bool hasUVCoordinates;
};

NAMESPACE_DEBRIS_END
