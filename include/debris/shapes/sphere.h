#pragma once

#include <core/geometry/abstractshape.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API Sphere : public AbstractShape {
public:
	Sphere(const Vector &position, float radius, const Transform &transform);
	~Sphere();
	bool intersect(RTCRay& ray) const;
	bool intersectP(RTCRay& ray) const;
	bool isInside(const Vector &p) const;
	Vector sampleArea(float u1, float u2, float &pdf) const;
	Vector getNormal(const Vector &point) const;
	void registerShape(RTCScene &scene) const;
	void updateShape(const RTCScene &scene, float time) const;
	float getRadius() const {
		return radius;
	}
	Vector getPosition() const {
		return local2world * Vector();
	}

private:
	float radius;
	Vector position;
	// This is used to keep track of time
	mutable float currentTime;
}; 

NAMESPACE_DEBRIS_END
