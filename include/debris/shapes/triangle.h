#pragma once

#include <core/geometry/abstractshape.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API Triangle : public AbstractShape {
public:
	Triangle(const Transform &transform, Vector *vertices, Eigen::Vector2f *uvs = nullptr);
	bool intersect(RTCRay &ray) const;
	bool intersectP(RTCRay &ray) const;
	void postIntersect(const RTCRay &ray, DifferentialGeometry &dg) const;
	bool isInside(const Vector &p) const;
	Vector sampleArea(float u1, float u2, float &pdf) const;
	Vector getNormal(const Vector &point) const;
	void registerShape(RTCScene &scene) const;
	void updateShape(const RTCScene &scene, float time) const;

private:
	void worldToLocal();

	Vector vertices[3];
	Eigen::Vector2f uvs[3];
	bool hasUVCoordinates;
	// Precomputed triangle area
	float triangle_area, inv_triangle_area;
};

NAMESPACE_DEBRIS_END
