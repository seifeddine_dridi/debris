#pragma once

#include <core/shading/shader.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API MirrorShader : public Shader {
public:
	MirrorShader(const shared_ptr<Texture> &texture, float kr, float emittance);
	void createBSDF(const DifferentialGeometry &dGeom, float wavelength, BSDFContainer &bsdfContainer) const;
	
private:
	float kr;
};

NAMESPACE_DEBRIS_END
