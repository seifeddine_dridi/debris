#pragma once

#include <core/shading/shader.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API GlassShader : public Shader {
public:
	GlassShader(const shared_ptr<Texture> &texture, float iorOut, float iorIn);
	void createBSDF(const DifferentialGeometry &dGeom, float wavelength, BSDFContainer &bsdfContainer) const;

private:
	float iorOut, iorIn;
};

NAMESPACE_DEBRIS_END
