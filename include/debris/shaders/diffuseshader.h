#pragma once

#include <core/shading/shader.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API DiffuseShader : public Shader {
public:
	DiffuseShader(const shared_ptr<Texture> &texture, float kd, float emittance);
	void createBSDF(const DifferentialGeometry &dGeom, float wavelength, BSDFContainer &bsdfContainer) const;

private:
	float kd;
};

NAMESPACE_DEBRIS_END
