#pragma once

#ifdef DEBRISCWRAPPER_EXPORTS
#define DEBRISCWRAPPER_API __declspec(dllexport)
#else
#define DEBRISCWRAPPER_API __declspec(dllimport)
#endif

// Opaque handles
typedef struct _Handle* Handle;
typedef struct _SceneHandle* SceneHandle;
typedef struct _LightHandle* LightHandle;
typedef struct _ShaderHandle* ShaderHandle;
typedef struct _TextureHandle* TextureHandle;
typedef struct _ShapeHandle* ShapeHandle;
typedef struct _PrimitiveHandle* PrimitiveHandle;
typedef struct _CameraHandle* CameraHandle;
typedef struct _RendererHandle* RendererHandle;
typedef struct _RenderingConfig* RenderingConfig;
typedef struct _FrameBufferHandle* FrameBufferHandle;

#ifdef __cplusplus
extern "C" {
#endif

DEBRISCWRAPPER_API void initEngine(int argc, char **argv);

// Scene loading
DEBRISCWRAPPER_API RenderingConfig parseSceneFile(const char *filename);
DEBRISCWRAPPER_API SceneHandle loadScene(const char *filename);

// Object instantiation
DEBRISCWRAPPER_API LightHandle createLight(const char *type);
DEBRISCWRAPPER_API TextureHandle createTexture(const char *type);
DEBRISCWRAPPER_API ShaderHandle createShader(const char *type);
DEBRISCWRAPPER_API ShaderHandle createTexturedShader(const char *type, TextureHandle texture);
DEBRISCWRAPPER_API ShapeHandle createShape(const char *type);
DEBRISCWRAPPER_API PrimitiveHandle createPrimitiveShape(ShapeHandle shape, ShaderHandle shader);
DEBRISCWRAPPER_API PrimitiveHandle createPrimitiveLight(LightHandle light);
DEBRISCWRAPPER_API CameraHandle createCamera(const char *type);
DEBRISCWRAPPER_API SceneHandle createScene(const char *type, PrimitiveHandle *shapes, int size);
DEBRISCWRAPPER_API RendererHandle createRenderer(const char *type);
DEBRISCWRAPPER_API RenderingConfig createRenderingConfig(const RendererHandle renderer, const CameraHandle camera, const SceneHandle scene);
DEBRISCWRAPPER_API FrameBufferHandle createFrameBuffer(const char *type, int width, int height);

DEBRISCWRAPPER_API void saveFramebuffer(FrameBufferHandle frameBuffer, const char *filepath);

// Handle modifiers
DEBRISCWRAPPER_API void setFloat1Attribute(Handle handle, const char *property, float x);
DEBRISCWRAPPER_API void setFloat3Attribute(Handle handle, const char *property, float x, float y, float z);
DEBRISCWRAPPER_API void setMatrix33Attribute(Handle handle, const char *property, float *mat);
DEBRISCWRAPPER_API void setStringAttribute(Handle handle, const char *property, const char *value);

DEBRISCWRAPPER_API void	deleteHandle(Handle handle);

// Rendering
DEBRISCWRAPPER_API int renderFrameFromSceneFile(const char* scenefile, const char *outputImage, int spp, unsigned nbThreads);
DEBRISCWRAPPER_API void renderFrame(RendererHandle renderer, CameraHandle camera, SceneHandle scene, FrameBufferHandle frameBuffer, int spp);
DEBRISCWRAPPER_API void renderFrame2(SceneHandle scene, const char *outputImage, int spp);

DEBRISCWRAPPER_API unsigned getMaximumThreadCount();

#ifdef __cplusplus
}
#endif
