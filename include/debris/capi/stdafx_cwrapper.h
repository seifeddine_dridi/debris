#pragma once

// stdafx_cwrapper.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#include <boost/timer/timer.hpp>
#include <boost/filesystem.hpp>

// core
#include <core/propertylist.h>
#include <core/film.h>
#include <core/scene.h>
#include <core/rendertaskmanager.h>
// cameras
#include <cameras/perspectivecamera.h>
// shapes
#include <shapes/sphere.h>
// lights
#include <lights/directionallight.h>
#include <lights/pointlight.h>
#include <lights/environmentlight.h>
// shaders
#include <shaders/diffuseshader.h>
#include <shaders/mirrorshader.h>
// integrators
#include <integrators/whittedintegrator.h>
// renderers
#include <renderers/defaultrenderer.h>
// XML parser
#include <parsers/xml/scenehandler.h>

using namespace debris;
using namespace boost;
