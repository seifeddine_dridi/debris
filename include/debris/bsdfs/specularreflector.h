#pragma once

#include <core/shading/bsdf.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API SpecularReflector : public BSDF {
public:
	SpecularReflector(const XYZColor &spectrum, float kr): BSDF(spectrum), kr(kr) {}
	XYZColor sample(Random &rng, Vector &wi, const Vector &wo, const Vector &normal) const;
	XYZColor eval(const Vector &wi, const Vector &wo, const Vector &normal) const;
	float getPdf(const Vector &wi, const Vector &wo, const Vector &normal) const;
	ReflectionType getType() const {
		return SPECULAR;
	}
	
private:
	float kr;
};

NAMESPACE_DEBRIS_END
