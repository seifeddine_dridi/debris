#pragma once

#include <core/shading/bsdf.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API Lambert : public BSDF {
public:
	Lambert(const XYZColor &spectrum, float kd): BSDF(spectrum), kd(kd) {}
	XYZColor sample(Random &rng, Vector &wi, const Vector &wo, const Vector &normal) const;
	XYZColor eval(const Vector &wi, const Vector &wo, const Vector &normal) const;
	float getPdf(const Vector &wi, const Vector &wo, const Vector &normal) const;
	ReflectionType getType() const {
		return DIFFUSE;
	}

private:
	float kd;
};

NAMESPACE_DEBRIS_END
