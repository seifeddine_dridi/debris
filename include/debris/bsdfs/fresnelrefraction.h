#pragma once

#include <core/shading/bsdf.h>

NAMESPACE_DEBRIS_BEGIN

class DEBRISCORE_API FresnelRefraction : public BSDF {
public:
	FresnelRefraction(const XYZColor &spectrum, float iorOut, float iorIn) : BSDF(spectrum), iorOut(iorOut), iorIn(iorIn) {}
	XYZColor sample(Random &rng, Vector &wi, const Vector &wo, const Vector &normal) const;
	XYZColor eval(const Vector &wi, const Vector &wo, const Vector &normal) const;
	float getPdf(const Vector &wi, const Vector &wo, const Vector &normal) const;
	ReflectionType getType() const {
		return SPECULAR;
	}

private:
	float iorOut, iorIn;
};

NAMESPACE_DEBRIS_END
