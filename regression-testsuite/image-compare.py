from pdiffer import PDiffer

nbTestFailures = 0
nbTestSuccesses = 0

def compare(image1, image2):
	pdiff = PDiffer(bin='perceptualdiff-1.1.1-win/perceptualdiff.exe')
	result = pdiff.pdiff(image1, image2, threshold = 1000, colorfactor = 0.0)
	if result:
		global nbTestFailures
		nbTestFailures += 1
		print 'Failure: ' + image1 + ' is different from ' + image2
	else:
		global nbTestSuccesses
		nbTestSuccesses += 1
		print 'Success: ' + image1 + ' is the same as ' + image2

compare('sphere.png', 'sphere-reference-image.png')
compare('sphere-checker-texture.png', 'sphere-checker-texture-reference-image.png')
compare('sphere-textured.png', 'sphere-textured-reference-image.png')
compare('sphere-depth-of-field.png', 'sphere-depth-of-field-reference-image.png')
compare('sphere-motion-blur.png', 'sphere-motion-blur-reference-image.png')
compare('triangle.png', 'triangle-reference-image.png')
compare('triangle-textured.png', 'triangle-textured-reference-image.png')
compare('uv-textured-triangle.png', 'uv-textured-triangle-reference-image.png')
compare('triangle-checker-texture.png', 'triangle-checker-texture-reference-image.png')
compare('triangle-motion-blur.png', 'triangle-motion-blur-reference-image.png')
compare('triangle-mesh.png', 'triangle-mesh-reference-image.png')
compare('textured-monkey.png', 'textured-monkey-reference-image.png')
compare('environment-light-test.png', 'environment-light-test-reference-image.png')
compare('fresnel-refraction-test.png', 'fresnel-refraction-test-reference-image.png')

print str(nbTestSuccesses) + ' tests succeeded, ' + str(nbTestFailures) + ' tests failed.'