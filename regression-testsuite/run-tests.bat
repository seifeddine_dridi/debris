rem add the path to the folder containing DebrisCLI.exe to environment variable "path"

DebrisCLI.exe -spp 1 sphere.xml sphere.png
DebrisCLI.exe -spp 1 sphere-checker-texture.xml sphere-checker-texture.png
DebrisCLI.exe -spp 1 sphere-textured.xml sphere-textured.png
DebrisCLI.exe -spp 32 sphere-depth-of-field.xml sphere-depth-of-field.png
DebrisCLI.exe -spp 32 sphere-motion-blur.xml sphere-motion-blur.png
DebrisCLI.exe -spp 1 triangle.xml triangle.png
DebrisCLI.exe -spp 1 triangle-textured.xml triangle-textured.png
DebrisCLI.exe -spp 1 uv-textured-triangle.xml uv-textured-triangle.png
DebrisCLI.exe -spp 1 triangle-checker-texture.xml triangle-checker-texture.png
DebrisCLI.exe -spp 32 triangle-motion-blur.xml triangle-motion-blur.png
DebrisCLI.exe -spp 1 triangle-mesh.xml triangle-mesh.png
DebrisCLI.exe -spp 1 textured-monkey.xml textured-monkey.png
DebrisCLI.exe -spp 32 environment-light-test.xml environment-light-test.png
DebrisCLI.exe -spp 1 fresnel-refraction-test.xml fresnel-refraction-test.png

rem Compare generated images with references
call python image-compare.py
pause